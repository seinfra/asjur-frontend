import { Permissao } from '../permissao/permissao';

export class Perfil {
    id: number;
    descricao: string;
    permissoes = new Array<PerfilPermissao>();
}

export class PerfilPermissao {
    id: number;
    permissao = new Permissao();

    constructor(permissao?: Permissao){
        this.permissao = permissao;
    }
}
