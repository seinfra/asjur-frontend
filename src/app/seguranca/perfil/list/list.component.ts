import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MenuItem, LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';

import { PerfilService, PerfilFiltro } from '../perfil.service';
import { Perfil } from '../perfil';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public items: MenuItem[];
  public quantidadeItemPorPagina: any[];
  public quantidadeItemPorPaginaSelecionada: any;
  public selecionados: Perfil[];
  public filtro = new PerfilFiltro;
  public totalRegistros = 0;
  public codigoEntidade: number;
  public consultar = true;
  public rotaNovo = "/asjur/perfil/novo";
  public rotaPadrao = '/asjur/perfil/';
  @ViewChild('tabela') tabelaEntidade;
  public homeBreadcrumb: MenuItem;

  constructor(
    public entidadeService: PerfilService,
    public title: Title,
    public toastr: ToastrService,
    public errorHandler: ErrorHandlerService,
    public confirmation: ConfirmationService,
    public router: Router,
    public segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'Asjur'},
      {label:'Cadastro'},
      {label:'Listagem de Perfis'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('Asjur - Perfil');
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  editar (codigo: number) {
    this.router.navigate([this.rotaPadrao + codigo]);
  }

  excluir (codigo: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
          this.entidadeService.excluir(codigo)
            .then(() => {
              if(this.tabelaEntidade.first === 0){
                this.pesquisar();
              }else {
                this.tabelaEntidade.first = 0;
              }
              this.toastr.success('Item excluído com sucesso!');
            })
            .catch (erro => this.errorHandler.handle(erro));
      })
    });
}

  limparFiltro () {
    this.filtro = new PerfilFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.id;
  }

}
