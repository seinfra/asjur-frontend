import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { Perfil, PerfilPermissao } from '../perfil';
import { PerfilService } from '../perfil.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Permissao } from '../../permissao/permissao';
import { PermissaoService } from '../../permissao/permissao.service';
import { SegurancaService } from '../../seguranca.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public entidade = new Perfil();
  public consulta: boolean;
  public codigoEntidade: number;
  public rotaListar = "/asjur/perfil/listar";
  public permissoes: Permissao[];
  public homeBreadcrumb: MenuItem;
  public permissoesSelecionadas: Permissao[] = [];

  //Campos para validação
  public descricao = true;

  constructor(
    public entidadeService: PerfilService,
    public permissaoService: PermissaoService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public title: Title,
    public segurancaService: SegurancaService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.items = [
      {label:'Asjur'},
      {label:'Segurança'},
      {label:'Perfil'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }else{
        this.carregarPermissoes();
    }
  }

  carregaSistemaPorSistema(sistemaSelecionado: number){
    this.permissaoService.permissaoPorSistema(sistemaSelecionado).then(dados => {
      this.permissoes = dados;
    }).catch(erro => this.errorHandler.handle(erro));
  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        this.preencherPermissaoPerfil();
        this.carregarPermissoes();
        if(this.consulta){
          this.title.setTitle (`asjur - Consulta de Perfil:${this.entidade.descricao}`);
        }else{
          this.title.setTitle (`asjur - Alteração de Perfil :${this.entidade.descricao}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number){}

  salvar (form: FormControl) {
    this.preencherPermissaoPerfilParaSalvar();
    if (this.entidade.id){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Perfil();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.descricao) {
      this.descricao = false;
    }else {
      this.descricao = true;
    }
  }

  carregarPermissoes() {
    this.permissaoService.listar()
      .then(dados => {
        this.permissoes = dados;
        this.removerPermissaoSelecionadaDeListaDePermissoes();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  preencherPermissaoPerfilParaSalvar(){

    this.entidade.permissoes = new Array<PerfilPermissao>();

    for(let i = 0; i < this.permissoesSelecionadas.length; i++) {
      let permissao: Permissao = this.permissoesSelecionadas[i];
      let entidadePermissao = new PerfilPermissao(permissao);
      this.entidade.permissoes.push(entidadePermissao);
    }

  }

  preencherPermissaoPerfil() {
    for(let i = 0; i < this.entidade.permissoes.length; i++) {
      let permissao = this.entidade.permissoes[i].permissao;
      this.permissoesSelecionadas.push(permissao);
    }
  }

  removerPermissaoSelecionadaDeListaDePermissoes(){
    for(let i = 0; i < this.permissoesSelecionadas.length; i++) {
      let permisao = this.permissoesSelecionadas[i]
      let index;
      for(let i=0; i < this.permissoes.length; i++){
        if(this.permissoes[i].id == permisao.id){
          index = this.permissoes.indexOf(this.permissoes[i]);
        }
      }
      this.permissoes.splice(index, 1);
    }

  }

}
