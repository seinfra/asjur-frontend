import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Usuario } from './usuario';

export class UsuarioFiltro {
  id: number;
  email: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends CrudService<Usuario>{

  private baseUrl = `${environment.apiUrl}/asjur-api/usuario`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/usuario`);
  }

  pesquisar(filtro: UsuarioFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.email) {
      params = params.append('email', filtro.email);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

}
