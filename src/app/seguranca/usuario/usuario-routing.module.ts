import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SegurancaGuard } from '../seguranca.guard';

const routes: Routes = [
    {
      path: 'asjur/usuario/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['USUARIO_CADASTRAR']}
    },
    {
      path: 'asjur/usuario/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['USUARIO_CONSULTAR']}
    },
    {
      path: 'asjur/usuario/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['USUARIO_ALTERAR']}
    },
    {
      path: 'asjur/usuario/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['USUARIO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class UsuarioRoutingModule { }
