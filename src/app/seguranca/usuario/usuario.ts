import { Permissao } from '../permissao/permissao';
import { Perfil } from '../perfil/perfil';

export class Usuario {
    id: number;
    email: string;
    nome: string;
    cpf: string;
    login: string;
    senha: string;
    perfil = new Perfil();
    entidade: any;
    permissoes = new Array<UsuarioPermissao>();

    constructor(){
      this.perfil = new Perfil();
    }
}

export class UsuarioPermissao {
    id: number;
    permissao = new Permissao();

    constructor(permissao?: Permissao){
        this.permissao = permissao;
    }
}
