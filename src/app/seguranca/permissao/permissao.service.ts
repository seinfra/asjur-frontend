import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { Permissao } from './permissao';
import { CrudService } from 'src/app/core/crud-service';

export class PermissaoFiltro {
  id: number;
  descricao: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class PermissaoService extends CrudService<Permissao>{

  private baseUrl = `${environment.apiUrl}/asjur-api/permissao`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/permissao`);
  }

  pesquisar(filtro: PermissaoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.descricao) {
      params = params.append('descricao', filtro.descricao);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  permissaoPorSistema(codigoSistema: number){
    console.log(codigoSistema);
    return this.http.get<any>(`${this.baseUrl}/permissaoPorSistema/${codigoSistema}`)
      .toPromise();
  }

}
