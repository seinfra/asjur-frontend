import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SegurancaGuard } from '../seguranca.guard';

const routes: Routes = [
    {
      path: 'asjur/permissao/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CADASTRAR']}
    },
    {
      path: 'asjur/permissao/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CONSULTAR']}
    },
    {
      path: 'asjur/permissao/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_ALTERAR']}
    },
    {
      path: 'asjur/permissao/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PERMISSAO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class permissaoRoutingModule { }
