import { DataLocaleService } from './../../core/data-locale-service';
import { Component, OnInit } from '@angular/core';

import { RelatoriosService } from './../relatorios.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-relatorio-controle-prazos',
  templateUrl: './relatorio-controle-prazos.component.html',
  styleUrls: ['./relatorio-controle-prazos.component.css']
})
export class RelatorioControlePrazoComponent implements OnInit {

  periodoInicio: Date;
  periodoFim: Date;
  ugb: string;
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public loading = false;
  public ugbs = [
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  constructor(public relatoriosService: RelatoriosService,
    public locale: DataLocaleService) { }

  ngOnInit() {
    this.locale;
    this.items = [
      {label:'SeinfraSin'},
      {label:'Relatório'},
      {label:'Controle de prazos'}
    ];

    this.periodoInicio = null;
    this.periodoFim = null;
    this.homeBreadcrumb = {icon: 'fa fa-home'};
  }

  limparFiltro () {
    this.periodoInicio = null;
    this.periodoFim = null;
    this.ugb = undefined;
  }

  gerar() {
    this.loading = true;
    this.relatoriosService.relatorioControlePrazos(this.periodoInicio, this.periodoFim, this.ugb)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.loading = false;
      });
  }
}
