import { NgxLoadingModule } from 'ngx-loading';
import { ComponentesModule } from './../componentes/componentes.module';
import { RelatorioControlePrazoComponent } from './relatorio-controle-prazos/relatorio-controle-prazos.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CalendarModule } from 'primeng/calendar';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {DropdownModule} from 'primeng/dropdown';

import { SharedModule } from './../shared/shared.module';
import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { RelatorioPlanoFeriasComponent } from './relatorio-ferias/relatorio-ferias.component';
import { RelatorioControleJuridicoComponent } from './relatorio-controle-juridico/relatorio-controle-juridico.component';
import { RelatorioControleJuridicoResumoComponent } from './relatorio-controle-juridico-resumo/relatorio-controle-juridico-resumo.component';
import { RelatorioControleEntregaComponent } from './relatorio-controle-entrega/relatorio-controle-entrega.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    CalendarModule,
    BreadcrumbModule,
    DropdownModule,
    SharedModule,
    RelatoriosRoutingModule,
    ComponentesModule,
    NgxLoadingModule,
  ],
  declarations: [
    RelatorioPlanoFeriasComponent,
    RelatorioControlePrazoComponent,
    RelatorioControleJuridicoComponent,
    RelatorioControleJuridicoResumoComponent,
    RelatorioControleEntregaComponent]
})
export class RelatoriosModule { }
