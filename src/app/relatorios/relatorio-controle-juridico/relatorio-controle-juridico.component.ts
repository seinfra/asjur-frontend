import { ErrorHandlerService } from './../../core/error-handler.service';
import { Funcionario } from 'src/app/recursos-humanos/funcionario/funcionario';
import { FuncionarioService } from './../../recursos-humanos/funcionario/funcionario.service';
import { DataLocaleService } from './../../core/data-locale-service';
import { Component, OnInit } from '@angular/core';

import { RelatoriosService } from './../relatorios.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-relatorio-controle-juridico',
  templateUrl: './relatorio-controle-juridico.component.html',
  styleUrls: ['./relatorio-controle-juridico.component.css']
})
export class RelatorioControleJuridicoComponent implements OnInit {

  public ano: number;
  public ugb: string;
  public mes: string;
  public tipo: string;
  public funcionario = new Funcionario();
  public funcionarios: Funcionario[];
  public prioridade: string;
  public loading = false;
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public ugbs = [
    { label: '--- Selecione ---', value: '' },
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  public tipos = [
    { label: '--- Selecione ---', value: '' },
    { label: 'POR ADVOGADO', value: 'ADVOGADO' },
    { label: 'POR COMPLEXIDADE', value: 'COMPLEXIDADE' },
  ]

  public prioridades = [
    { label: '--- Selecione ---', value: '' },
    { label: 'ALTISSÍMA', value: 'ALTISSIMA' },
    { label: 'ALTA', value: 'ALTA' },
    { label: 'MÉDIA', value: 'MEDIA' },
    { label: 'BAIXA', value: 'BAIXA' },
  ]

  public meses = [
    { label: '--- Selecione ---', value: '' },
    { label: 'JANEIRO', value: '1' },
    { label: 'FEVEREIRO', value: '2' },
    { label: 'MARÇO', value: '3' },
    { label: 'ABRIL', value: '4' },
    { label: 'MAIO', value: '5' },
    { label: 'JUNHO', value: '6' },
    { label: 'JULHO', value: '7' },
    { label: 'AGOSTO', value: '8' },
    { label: 'SETEMBRO', value: '9' },
    { label: 'OUTUBRO', value: '10' },
    { label: 'NOVEMBRO', value: '11' },
    { label: 'DEZEMBRO', value: '12' },
  ]

  constructor(
    public relatoriosService: RelatoriosService,
    public errorHandler: ErrorHandlerService,
    public funcionarioService: FuncionarioService,
    public locale: DataLocaleService) { }

  ngOnInit() {
    this.locale;
    this.items = [
      {label:'SeinfraSin'},
      {label:'Relatório'},
      {label:'Controle de Jurídico'}
    ];

    this.listarFuncionario();

    this.homeBreadcrumb = {icon: 'fa fa-home'};
  }

  limparFiltro () {
    this.ano = null;
    this.ugb = null;
    this.mes = null;
    this.tipo = null;
    this.prioridade = null;
    this.funcionario = new Funcionario();
    this.listarFuncionario();
  }

  listarFuncionario(){
    this.funcionarioService.listar()
    .then( element => {
      this.funcionarios = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  gerar() {
    this.loading = true;
    this.relatoriosService.relatorioControleJuridico(this.tipo, this.ano, this.mes, this.ugb, this.prioridade, this.funcionario)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.loading = false;
      });
  }
}
