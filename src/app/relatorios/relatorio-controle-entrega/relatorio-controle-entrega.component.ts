import { ErrorHandlerService } from './../../core/error-handler.service';
import { Funcionario } from 'src/app/recursos-humanos/funcionario/funcionario';
import { FuncionarioService } from './../../recursos-humanos/funcionario/funcionario.service';
import { DataLocaleService } from './../../core/data-locale-service';
import { Component, OnInit } from '@angular/core';

import { RelatoriosService } from './../relatorios.service';
import { MenuItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-relatorio-controle-entrega',
  templateUrl: './relatorio-controle-entrega.component.html',
  styleUrls: ['./relatorio-controle-entrega.component.css']
})
export class RelatorioControleEntregaComponent implements OnInit {

  periodoInicio: Date;
  periodoFim: Date;
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public loading = false;
  public funcionarios: Funcionario[] = [];
  public funcionario = new Funcionario();

  constructor(
    public relatoriosService: RelatoriosService,
    public errorHandler: ErrorHandlerService,
    public toastr: ToastrService,
    public funcionarioService: FuncionarioService,
    public locale: DataLocaleService) { }

  ngOnInit() {
    this.locale;
    this.items = [
      {label:'SeinfraSin'},
      {label:'Relatório'},
      {label:'Controle de prazos'}
    ];
    this.listarFuncionario();
    this.periodoInicio = null;
    this.periodoFim = null;
    this.homeBreadcrumb = {icon: 'fa fa-home'};
  }

  limparFiltro () {
    this.periodoInicio = null;
    this.periodoFim = null;
    this.funcionario = new Funcionario();
    this.listarFuncionario();
  }

  listarFuncionario() {
    this.funcionarioService.funcionarioJuridico()
    .then( element => {
      this.funcionarios = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  gerar() {
    if(!this.funcionario.id){
      this.toastr.error('Campo funcionário é obrigatório!')
    }else{
      this.loading = true;

      this.relatoriosService.relatorioControleEntrega(this.periodoInicio, this.periodoFim, this.funcionario.id)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.loading = false;
      });
    }
  }
}
