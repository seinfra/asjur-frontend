import { ErrorHandlerService } from './../../core/error-handler.service';
import { DataLocaleService } from './../../core/data-locale-service';
import { Component, OnInit } from '@angular/core';

import { RelatoriosService } from './../relatorios.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-relatorio-controle-juridico-resumo',
  templateUrl: './relatorio-controle-juridico-resumo.component.html',
  styleUrls: ['./relatorio-controle-juridico-resumo.component.css']
})
export class RelatorioControleJuridicoResumoComponent implements OnInit {

  public ano: number;
  public ugb: string;
  public tipo: string;
  public items: MenuItem[];
  public loading = false;
  public homeBreadcrumb: MenuItem;
  public ugbs = [
    { label: '--- Selecione ---', value: '' },
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  public tipos = [
    { label: '--- Selecione ---', value: '' },
    { label: 'POR ADVOGADO', value: 'ADVOGADO' },
    { label: 'POR COMPLEXIDADE', value: 'COMPLEXIDADE' },
  ]

  constructor(
    public relatoriosService: RelatoriosService,
    public errorHandler: ErrorHandlerService,
    public locale: DataLocaleService) { }

  ngOnInit() {
    this.locale;
    this.items = [
      {label:'SeinfraSin'},
      {label:'Relatório'},
      {label:'Controle de Jurídico'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};
  }

  limparFiltro () {
    this.ano = null;
    this.ugb = null;
    this.tipo = null;

  }

  gerar() {
    this.loading = true;
    this.relatoriosService.relatorioControleJuridicoResumo(this.tipo, this.ano, this.ugb)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.loading = false;
      });
  }
}
