import { DataLocaleService } from './../../core/data-locale-service';
import { Component, OnInit } from '@angular/core';

import { RelatoriosService } from './../relatorios.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-relatorio-ferias',
  templateUrl: './relatorio-ferias.component.html',
  styleUrls: ['./relatorio-ferias.component.css']
})
export class RelatorioPlanoFeriasComponent implements OnInit {

  periodoInicio: Date;
  periodoFim: Date;
  ano: number;
  ugb: string;
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public loading = false;
  public ugbs = [
    { label: 'APOIO', value: 'APOIO' },
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  constructor(public relatoriosService: RelatoriosService,
              public locale: DataLocaleService) { }

  ngOnInit() {
    this.locale;

    this.items = [
      {label:'SeinfraSin'},
      {label:'Relatório'},
      {label:'Plano de férias'}
    ];

    this.periodoInicio = null;
    this.periodoFim = null;
    this.homeBreadcrumb = {icon: 'fa fa-home'};
  }

  limparFiltro () {
    this.periodoInicio = null;
    this.periodoFim = null;
    this.ano = undefined;
    this.ugb = undefined;
  }

  gerar() {
    this.loading = true;
    this.relatoriosService.relatorioPlanoFerias(this.ano, this.periodoInicio, this.periodoFim, this.ugb)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.loading = false;
      });
  }
}
