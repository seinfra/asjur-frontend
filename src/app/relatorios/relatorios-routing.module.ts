import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatorioPlanoFeriasComponent } from './relatorio-ferias/relatorio-ferias.component';
import { RelatorioControlePrazoComponent } from './relatorio-controle-prazos/relatorio-controle-prazos.component';
import { RelatorioControleJuridicoComponent } from './relatorio-controle-juridico/relatorio-controle-juridico.component';
import { RelatorioControleJuridicoResumoComponent } from './relatorio-controle-juridico-resumo/relatorio-controle-juridico-resumo.component';
import { RelatorioControleEntregaComponent } from './relatorio-controle-entrega/relatorio-controle-entrega.component';

const routes: Routes = [
  {
      path: 'asjur/relatorio/planoFerias',
      component: RelatorioPlanoFeriasComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['RELATORIO_PLANO_FERIAS']}
  },
  {
    path: 'asjur/relatorio/controleJuridico',
    component: RelatorioControleJuridicoComponent,
    canActivate:[SegurancaGuard],
    data: { roles: ['RELATORIO_CONTROLE_JURIDICO']}
  },
  {
    path: 'asjur/relatorio/controlePrazos',
    component: RelatorioControlePrazoComponent,
    canActivate:[SegurancaGuard],
    data: { roles: ['RELATORIO_CONTROLE_PRAZO']}
},
{
  path: 'asjur/relatorio/controleEntrega',
  component: RelatorioControleEntregaComponent,
  canActivate:[SegurancaGuard],
  data: { roles: ['RELATORIO_CONTROLE_ENTREGA']}
},
{
  path: 'asjur/relatorio/controleJuridicoResumo',
  component: RelatorioControleJuridicoResumoComponent,
  canActivate:[SegurancaGuard],
  data: { roles: ['RELATORIO_CONTROLE_JURIDICO_RESUMO']}
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }
