import { FuncionarioService } from './../recursos-humanos/funcionario/funcionario.service';
import { Funcionario } from 'src/app/recursos-humanos/funcionario/funcionario';
import { PlanoFerias } from './../core/model/planoFerias';
import { CrudService } from './../core/crud-service';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { environment } from './../../environments/environment';

@Injectable()
export class RelatoriosService extends CrudService<PlanoFerias>{

  private baseUrl = `${environment.apiUrl}/asjur-api/relatorios`;

  constructor(protected http: SistemaHttp, private funcionarioService: FuncionarioService) {
    super(http, `${environment.apiUrl}/asjur-api/relatorios`);
  }

  relatorioPlanoFerias(ano: number, inicio: Date, fim: Date, ugb: string) {

    const params = new HttpParams()
      .append('ano', ano !== undefined ? ano.toString() : '')
      .append('inicio', inicio !== null ? moment(inicio).format('YYYY-MM-DD') : '')
      .append('fim', fim !== null ? moment(fim).format('YYYY-MM-DD') : '')
      .append('ugb', ugb !== undefined ? ugb : '');

    return this.http.get(`${this.baseUrl}/planoFerias`,
      { params, responseType: 'blob' })
      .toPromise();
  }

  relatorioControleJuridico(tipo: string, ano: number, mes: string, ugb: string, prioridade: string, funcionario: Funcionario) {

    const params = new HttpParams()
    .append('tipo', tipo !== undefined ? tipo : '')
    .append('ano', ano !== undefined ? ano.toString() : '')
    .append('mes', mes !== undefined ? mes : '')
    .append('ugb', ugb !== undefined ? ugb : '')
    .append('prioridade', prioridade !== undefined ? prioridade : '')
    .append('id', funcionario !== null && funcionario !== undefined && funcionario.id !== undefined? funcionario.id.toString() : '');

    return this.http.get(`${this.baseUrl}/controleJuridico`,
      { params, responseType: 'blob' })
      .toPromise();
  }

  relatorioControleJuridicoResumo(tipo: string, ano: number, ugb: string) {

    const params = new HttpParams()
    .append('tipo', tipo !== undefined ? tipo : '')
    .append('ano', ano !== undefined ? ano.toString() : '')
    .append('ugb', ugb !== undefined ? ugb : '')

    return this.http.get(`${this.baseUrl}/controleJuridicoResumo`,
      { params, responseType: 'blob' })
      .toPromise();
  }

  relatorioControlePrazos(inicio: Date, fim: Date, ugb: string) {
    const params = new HttpParams()
    .append('inicio', inicio !== null ? moment(inicio).format('YYYY-MM-DD') : '')
    .append('fim', fim !== null ? moment(fim).format('YYYY-MM-DD') : '')
    .append('ugb', ugb !== undefined ? ugb : '');

    return this.http.get(`${this.baseUrl}/controlePrazos`,
      { params, responseType: 'blob' })
      .toPromise();
  }

  relatorioControleEntrega(inicio: Date, fim: Date, id: number) {
    const params = new HttpParams()
    .append('inicio', inicio !== null ? moment(inicio).format('YYYY-MM-DD') : '')
    .append('fim', fim !== null ? moment(fim).format('YYYY-MM-DD') : '')
    .append('id', id !== undefined ? id.toString() : '');

    return this.http.get(`${this.baseUrl}/controleEntrega`,
      { params, responseType: 'blob' })
      .toPromise();
  }
}
