export class CheckList {
  id: number;
  descricao: string;
  checkListTipos = new Array<CheckListTipo>();

  constructor(id?: number, descricao?: string){
    this.id = id;
    this.descricao = descricao;
  }
}

export class CheckListTipo {
  id: number;
  descricao: string;
  checkListSubs = new Array<CheckListSub>();

  constructor(id?: number, descricao?: string){
    this.id = id;
    this.descricao = descricao;
  }
}

export class CheckListSub {
  id: number;
  descricao: string;
  complexidade: string;
  prazo: number;
  checkList = new CheckList();
  checkListDocumentos = new Array<CheckListDocumento>();

  constructor(id?: number, descricao?: string, complexidade?: string, prazo?: number){
    this.id = id;
    this.complexidade = complexidade;
    this.prazo = prazo;
    this.descricao = descricao;
  }
}

export class CheckListDocumento {
  id: number;
  descricao: string;
  ativo: boolean;
  checkListSub = new CheckListSub();

  constructor(id?: number, descricao?: string){
    this.id = id;
    this.descricao = descricao;
  }
}




