import { environment } from '../../../environments/environment';
import { CrudService } from '../../core/crud-service';
import { Injectable } from '@angular/core';
import { SistemaHttp } from '../../seguranca/sistema-http';
import { CheckListDocumentoPendente } from 'src/app/core/model/check-list-documento';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CheckListDocumentoService extends CrudService<CheckListDocumentoPendente>{

  private baseUrl = `${environment.apiUrl}/asjur-api/checklist-documento`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/checklist-documento`);
  }

  buscarPorDocumento (id: number, documento: number): Promise<any> {
    const params = new HttpParams()
    .append('id', id !== undefined ? id.toString() : '')
    .append('documento', documento !== undefined ? documento.toString() : '');

    return this.http.get(`${this.baseUrl}/checklist`, { params })
      .toPromise()
      .then( response => response);
  }

  buscarPorDocumentoPendente (id: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/documento/${id}`)
      .toPromise()
      .then( response => response);
  }

  buscarPorDocumentoPendenteExcluir (id: number): Promise<any> {
    return this.http.delete(`${this.baseUrl}/${id}`)
      .toPromise()
      .then( response => response);
  }

}
