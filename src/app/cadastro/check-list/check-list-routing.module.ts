
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/checklist/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CHECKLIST_CADASTRAR']}
    },
    {
      path: 'asjur/checklist/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CHECKLIST_CONSULTAR']}
    },
    {
      path: 'asjur/checklist/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CHECKLIST_ALTERAR']}
    },
    {
      path: 'asjur/checklist/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CHECKLIST_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CheckListRoutingModule { }
