import { CheckListService } from './../check-list.service';
import { CheckList, CheckListSub, CheckListDocumento, CheckListTipo } from './../check-list';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public entidade = new CheckList();
  public consulta: boolean;
  public codigoEntidade: number;
  public rotaListar = "/asjur/checklist/listar";
  public subSelecionados: CheckListSub[] = [];
  public tipoSelecionados: CheckListTipo[] = [];
  public documentoSelecionados: CheckListDocumento[] = [];
  public documentoCheckList: CheckListDocumento[] = [];
  public checkListSub = new CheckListSub();
  public checkListTipoSub = new CheckListTipo();
  public checkListTipo = new CheckListTipo();
  public checkListSubDoc = new CheckListSub();
  public checkListDocumento = new CheckListDocumento();
  public visibleDialogTipo: boolean;
  public visibleDialogSub: boolean;
  public visibleDialogDocumentacao: boolean;
  public indexSub: number;
  public indexTipo: number;
  public indexDocumentacao: number;
  public editandoTipo: boolean;
  public editandoSub: boolean;
  public editandoDocumentacao: boolean;
  public valorTotalPedido = 0;
  public homeBreadcrumb: MenuItem;
  public prioridades = [
    { label: 'ALTISSÍMA', value: 'ALTISSIMA' },
    { label: 'ALTA', value: 'ALTA' },
    { label: 'MÉDIA', value: 'MEDIA' },
    { label: 'BAIXA', value: 'BAIXA' },
  ]

  //Campos para validação
  public descricao = true;

  constructor(
    public entidadeService: CheckListService,
    public route: ActivatedRoute,
    public segurancaService: SegurancaService,
    public router: Router,
    public toastr: ToastrService,
    public confirmation: ConfirmationService,
    public title: Title,
    public errorHandler : ErrorHandlerService,
    public locale: DataLocaleService,
  ) { }

  ngOnInit() {
    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'CheckList'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};
    this.locale;
    this.valorTotalPedido;
    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }
  }

  buscarPrazo(complexidade: string){
    switch (complexidade) {
      case 'ALTISSIMA':
      this.checkListSub.prazo = 30;
          break;
      case 'ALTA':
      this.checkListSub.prazo = 15;
          break;
      case 'MEDIA':
      this.checkListSub.prazo = 10;
          break;
      case 'BAIXA':
      this.checkListSub.prazo = 5;
      break;
    }
  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        this.preencherTipoSalvo();
        if(this.consulta){
          this.title.setTitle (`SeinfraSin - Consulta de CheckList:${this.entidade.descricao}`);
        }else{
          this.title.setTitle (`SeinfraSin - Alteração de CheckList :${this.entidade.descricao}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number){}

  salvar (form: FormControl) {
    this.preencherTipoParaSalvar();
    if (this.entidade.id){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new CheckList();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){
    if(!form.value.descricao) {
      this.descricao = false;
    }else {
      this.descricao = true;
    }
  }

    // ------------------------------------------------------------------------------Tipo
    salvarTipo(checkListTipo: CheckListTipo) {
      if(checkListTipo.descricao){

        if(this.checkListTipoSub === checkListTipo){
          checkListTipo.checkListSubs = this.checkListTipoSub.checkListSubs;
        }else{
          checkListTipo.checkListSubs = this.subSelecionados;
          this.checkListTipoSub.checkListSubs = this.subSelecionados;
        }
        if(this.editandoTipo){
          this.tipoSelecionados[this.indexTipo] = checkListTipo;
          this.checkListTipoSub.checkListSubs = this.subSelecionados;
        }else{
          this.tipoSelecionados.push(checkListTipo);
        }
        this.checkListTipo = new CheckListTipo();
        this.visibleDialogTipo = false;
        this.editandoTipo = false;

      }else{
          this.toastr.error('Informe a descrição!');
          this.descricao = false;
      }
    }

    editarTipo(checkListTipo: CheckListTipo, index: number) {
      this.checkListTipo = new CheckListTipo(checkListTipo.id, checkListTipo.descricao);

      this.subSelecionados = Array<CheckListSub>();

      checkListTipo.checkListSubs.forEach( element => {
        this.subSelecionados.push(element);
      })

      this.indexTipo = index;
      this.editandoTipo = true
      this.visibleDialogTipo = true;
    }

    removerTipo(index: number) {
      this.confirmation.confirm({
        message: 'Tem certeza que deseja excluir',
        accept: (() => {
          this.subSelecionados.splice(index, 1);
          this.toastr.success('Item excluído com sucesso!');
        })
      });
    }

    preencherTipoParaSalvar(){
      this.entidade.checkListTipos = new Array<CheckListTipo>();

      this.tipoSelecionados.forEach( checkListTipo => {
        this.entidade.checkListTipos.push(checkListTipo);
      });
    }

    preencherTipoSalvo() {
      this.entidade.checkListTipos.forEach( checkListTipo => {
        checkListTipo.checkListSubs.forEach(element => {
          this.subSelecionados.push(element);
        });
      this.tipoSelecionados.push(checkListTipo);
      });
    }

    showDialogTipo(){
      this.checkListTipo = new CheckListTipo();
      this.subSelecionados = Array<CheckListSub>();
      this.visibleDialogTipo = true;
    }

    cancelarModalTipo(){
      this.visibleDialogTipo = false;
    }

  // ------------------------------------------------------------------------------SubTipo
  salvarSub(checkListSub: CheckListSub, checkListTipo: CheckListTipo) {
    this.checkListTipoSub = new CheckListTipo();
    this.checkListTipoSub = checkListTipo;
    if(checkListSub.descricao || checkListSub.complexidade || checkListSub.prazo){

      if(this.editandoSub){
        this.subSelecionados[this.indexSub] = checkListSub;
      }else{
        this.subSelecionados.push(checkListSub);
      }

      if(this.checkListSubDoc === checkListSub){
        checkListSub.checkListDocumentos = this.checkListSubDoc.checkListDocumentos;
        this.checkListSubDoc.checkListDocumentos = this.documentoSelecionados;
        this.checkListTipoSub.checkListSubs = this.subSelecionados;
      }else{
        checkListSub.checkListDocumentos = this.documentoSelecionados;
        this.checkListSubDoc.checkListDocumentos = this.documentoSelecionados;
        this.checkListTipoSub.checkListSubs = this.subSelecionados;
      }
      this.checkListSub = new CheckListSub();
      this.visibleDialogSub = false;
      this.editandoSub = false;

    }else{
        this.toastr.error('Informe a descrição/Complexidade/Prazo!');
        this.descricao = false;
    }
  }

  editarSub(checkListSub: CheckListSub, index: number) {
    this.checkListSub = new CheckListSub(checkListSub.id, checkListSub.descricao,
    checkListSub.complexidade, checkListSub.prazo);

    this.documentoSelecionados = Array<CheckListDocumento>();

    checkListSub.checkListDocumentos.forEach( element => {
      this.documentoSelecionados.push(element);
    })

    this.indexSub = index;
    this.editandoSub = true
    this.visibleDialogSub = true;
  }

  removerSub(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.subSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  showDialogSub(){
    this.checkListSub = new CheckListSub();
    this.documentoSelecionados = Array<CheckListDocumento>();
    this.visibleDialogSub = true;
  }

  cancelarModalSub(){
    this.visibleDialogSub = false;
  }
// --------------------------------------------------------------------documentação

salvarDocumentacao(checkListDocumento: CheckListDocumento, checkListSub: CheckListSub) {
  this.checkListSubDoc = new CheckListSub();
  this.checkListSubDoc = checkListSub;
  if(checkListDocumento.descricao){

    if(this.editandoDocumentacao){
      this.documentoSelecionados[this.indexDocumentacao] = new CheckListDocumento(checkListDocumento.id, checkListDocumento.descricao);
      this.checkListSubDoc.checkListDocumentos = this.documentoSelecionados;
    }else{
      this.documentoSelecionados.push(checkListDocumento);
      this.checkListSubDoc.checkListDocumentos = this.documentoSelecionados;
    }
    this.checkListDocumento = new CheckListDocumento();
    this.visibleDialogDocumentacao = false;
    this.editandoDocumentacao = false;

  }else{
      this.toastr.error('Informe a descrição!');
      this.descricao = false;
  }
}

editarDocumentacao(checkListDocumento: CheckListDocumento, index: number) {
  this.checkListDocumento = new CheckListDocumento(checkListDocumento.id, checkListDocumento.descricao);
  this.indexDocumentacao = index;
  this.editandoDocumentacao = true
  this.visibleDialogDocumentacao = true;
}

removerDocumentacao(index: number) {
  this.confirmation.confirm({
    message: 'Tem certeza que deseja excluir',
    accept: (() => {
      this.documentoSelecionados.splice(index, 1);
      this.toastr.success('Item excluído com sucesso!');
    })
  });
}

showDialogDocumentacao(){
  this.checkListDocumento = new CheckListDocumento();
  this.visibleDialogDocumentacao = true;
}

cancelarModalDocumentacao(){
  this.visibleDialogDocumentacao = false;
}

}
