import { environment } from './../../../environments/environment';
import { CrudService } from './../../core/crud-service';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { CheckList } from './check-list';
import { SistemaHttp } from '../../seguranca/sistema-http';

export class CheckListFiltro {
  id: number;
  descricao: string;
  complexidade: string
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class CheckListService extends CrudService<CheckList>{

  private baseUrl = `${environment.apiUrl}/asjur-api/checklist`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/checklist`);
  }

  pesquisar(filtro: CheckListFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('codigo', filtro.id.toString());
    }

    if(filtro.descricao) {
      params = params.append('nome', filtro.descricao);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  funcionarioPorCpf(cpf: string): Promise<any> {
    return this.http.get(`${this.baseUrl}/funcionario/${cpf}`)
      .toPromise()
      .then( response => response);
  }

  buscarTipo (codigo: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/buscarTipo/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  buscarSub (codigo: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/buscarSub/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  buscarSubSelecionado (codigo: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/buscarSubSelecionado/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  buscarDocumentacao (id: number, documento: number): Promise<any> {
    const params = new HttpParams()
    .append('id', id !== undefined ? id.toString() : '')
    .append('documento', documento !== undefined ? documento.toString() : '');

    return this.http.get(`${this.baseUrl}/buscarDocumentacao`, { params })
      .toPromise()
      .then( response => response);
  }

}
