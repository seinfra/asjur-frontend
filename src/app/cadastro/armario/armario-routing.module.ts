import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/armario/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ARMARIO_CADASTRAR']}
    },
    {
      path: 'asjur/armario/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ARMARIO_CONSULTAR']}
    },
    {
      path: 'asjur/armario/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ARMARIO_ALTERAR']}
    },
    {
      path: 'asjur/armario/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ARMARIO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ArmarioRoutingModule { }
