import { ArmarioFiltro, ArmarioService } from './../armario.service';
import { Armario } from './../armario';
import { TipoDocumentoFiltro } from './../../tipo-documento/tipo-documento.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MenuItem, LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public quantidadeItemPorPagina: any[];
  public quantidadeItemPorPaginaSelecionada: any;
  public selecionados: Armario[];
  public filtro = new ArmarioFiltro;
  public totalRegistros = 0;
  public codigoEntidade: number;
  public consultar = true;
  public rotaNovo = "/asjur/armario/novo";
  public rotaPadrao = '/asjur/armario/';
  @ViewChild('tabela') tabelaEntidade;

  constructor(
    public entidadeService: ArmarioService,
    public title: Title,
    public toastr: ToastrService,
    public errorHandler: ErrorHandlerService,
    public confirmation: ConfirmationService,
    public router: Router,
    public segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Listagem de Armário'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('SeinfraSin - Armário');
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  editar (codigo: number) {
    this.router.navigate([this.rotaPadrao + codigo]);
  }

  excluir (codigo: number) {
      this.confirmation.confirm({
        message: 'Tem certeza que deseja excluir',
        accept: (() => {
            this.entidadeService.excluir(codigo)
              .then(() => {
                if(this.tabelaEntidade.first === 0){
                  this.pesquisar();
                }else {
                  this.tabelaEntidade.first = 0;
                }
                this.toastr.success('Item excluído com sucesso!');
              })
              .catch (erro => this.errorHandler.handle(erro));
        })
      });
  }

  limparFiltro () {
    this.filtro = new ArmarioFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.id;
  }

}
