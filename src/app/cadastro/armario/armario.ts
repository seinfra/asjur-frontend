import { UnidadeAdministrativa } from './../unidade-administrativa/unidade-administrativa';

export class Armario {
    id: number;
    nome: string;
    prateleira: string;
    unidadeAdministrativa = new UnidadeAdministrativa();
}
