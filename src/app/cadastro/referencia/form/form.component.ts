import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Referencia } from '../referencia';
import { ReferenciaService } from '../referencia.service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public entidade = new Referencia();
  public consulta: boolean;
  public codigoEntidade: number;
  public rotaListar = "/asjur/referencia/listar";

  //Campos para validação
  public nome = true;

  constructor(
    public entidadeService: ReferenciaService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public title: Title,
    public segurancaService: SegurancaService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Referência'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }

  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        if(this.consulta){
          this.title.setTitle (`SeinfraSin - Consulta de Referência:${this.entidade.descricao}`);
        }else{
          this.title.setTitle (`SeinfraSin - Alteração de Referência :${this.entidade.descricao}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number){}

  salvar (form: FormControl) {
    if (this.entidade.id){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Referencia();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

  }

}
