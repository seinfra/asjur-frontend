import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/referencia/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['REFERENCIA_CADASTRAR']}
    },
    {
      path: 'asjur/referencia/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['REFERENCIA_CONSULTAR']}
    },
    {
      path: 'asjur/referencia/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['REFERENCIA_ALTERAR']}
    },
    {
      path: 'asjur/referencia/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['REFERENCIA_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ReferenciaRoutingModule { }
