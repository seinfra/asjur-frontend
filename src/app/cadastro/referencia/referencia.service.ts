import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Referencia } from './referencia';

export class ReferenciaFiltro {
  id: number;
  descricao: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class ReferenciaService extends CrudService<Referencia>{

  private baseUrl = `${environment.apiUrl}/asjur-api/referencia`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/referencia`);
  }

  pesquisar(filtro: ReferenciaFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.descricao) {
      params = params.append('descricao', filtro.descricao);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  buscarPorDescricao(filtro: string){
    let params = new HttpParams();
    params = params.append('descricao', filtro);
    return this.http.get<any>(`${this.baseUrl}/buscarPorDescricao`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

}
