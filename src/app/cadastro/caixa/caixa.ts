import { UnidadeAdministrativa } from './../unidade-administrativa/unidade-administrativa';

export class Caixa {
    id: number;
    nome: string;
    ano: number;
    numero: number;
    unidadeAdministrativa = new UnidadeAdministrativa();
}
