import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/caixa/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CAIXA_CADASTRAR']}
    },
    {
      path: 'asjur/caixa/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CAIXA_CONSULTAR']}
    },
    {
      path: 'asjur/caixa/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CAIXA_ALTERAR']}
    },
    {
      path: 'asjur/caixa/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['CAIXA_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class CaixaRoutingModule { }
