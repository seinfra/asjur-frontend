import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Caixa } from './caixa';

export class CaixaFiltro {
  id: number;
  nome: string;
  numero: number;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class CaixaService extends CrudService<Caixa>{

  private baseUrl = `${environment.apiUrl}/asjur-api/caixa`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/caixa`);
  }

  pesquisar(filtro: CaixaFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    if(filtro.numero) {
      params = params.append('numero', filtro.numero.toString());
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  buscarPorNome(filtro: string){
    let params = new HttpParams();
    params = params.append('nome', filtro);
    return this.http.get<any>(`${this.baseUrl}/buscarPorNome`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

}
