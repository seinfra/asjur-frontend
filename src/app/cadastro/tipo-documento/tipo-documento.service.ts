import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { TipoDocumento } from './tipo-documento';

export class TipoDocumentoFiltro {
  id: number;
  descricao: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService extends CrudService<TipoDocumento>{

  private baseUrl = `${environment.apiUrl}/asjur-api/tipo-documento`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/tipo-documento`);
  }

  pesquisar(filtro: TipoDocumentoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.descricao) {
      params = params.append('descricao', filtro.descricao);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  buscarPorNome(filtro: string){
    let params = new HttpParams();
    params = params.append('nome', filtro);
    return this.http.get<any>(`${this.baseUrl}/buscarPorNome`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }


}
