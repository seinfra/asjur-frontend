import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/tipo-documento/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['TIPO_DOCUMENTO_CADASTRAR']}
    },
    {
      path: 'asjur/tipo-documento/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['TIPO_DOCUMENTO_CONSULTAR']}
    },
    {
      path: 'asjur/tipo-documento/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['TIPO_DOCUMENTO_ALTERAR']}
    },
    {
      path: 'asjur/tipo-documento/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['TIPO_DOCUMENTO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class TipoDocumentoRoutingModule { }
