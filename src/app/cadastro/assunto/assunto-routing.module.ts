import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/assunto/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ASSUNTO_CADASTRAR']}
    },
    {
      path: 'asjur/assunto/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ASSUNTO_CONSULTAR']}
    },
    {
      path: 'asjur/assunto/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ASSUNTO_ALTERAR']}
    },
    {
      path: 'asjur/assunto/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ASSUNTO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AssuntoRoutingModule { }
