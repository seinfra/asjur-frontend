import { Documento } from './../documento/documento';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import * as moment from 'moment';
import { DocumentoJuridico } from './documento-juridico';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';

export class DocumentoJuridicoFiltro {
  id: number;
  numeroDocumento: string;
  numeroProcesso: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class DocumentoJuridicoService extends CrudService<DocumentoJuridico>{

  private baseUrl = `${environment.apiUrl}/asjur-api/documento-juridico`;

  public selecionados: any;
  public contAltissima = 0;
  public contAlta = 0;
  public contMedia = 0;
  public contBaixa = 0;

  constructor(protected http: SistemaHttp, private segurancaService: SegurancaService) {
    super(http, `${environment.apiUrl}/asjur-api/documento-juridico`)
  }

  pesquisar(filtro: DocumentoJuridicoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.numeroDocumento) {
      params = params.append('numeroDocumento', filtro.numeroDocumento);
    }

    if(filtro.numeroProcesso) {
      params = params.append('numeroProcesso', filtro.numeroProcesso);
    }

    params = params.append('cpf', this.segurancaService.jwtPayload.cpf);

    return this.http.get<any>(this.baseUrl, { params })
    .toPromise()
    .then( response => {
      const resultado = {
        selecionados: response.content,
        total: response.totalElements
       }
      return resultado;
    });
  }

  incluirDocumento(entidade: DocumentoJuridico): Promise<any> {
    return this.http.post<any>(`${this.baseUrl}/incluirDocumento`, entidade)
      .toPromise();
  }

  alterarDocumento(entidade: DocumentoJuridico): Promise<any> {
    return this.http.put<any>(`${this.baseUrl}/alterar-documento/${entidade.id}`, entidade)
      .toPromise();
  }

  listaDocumentoPorUsuario (cpf: string): Promise<any> {
    return this.http.get(`${this.baseUrl}/usuario/${cpf}`)
      .toPromise()
      .then( response => response);
  }

  listaDocumentos(cpf: string): Promise<any> {

    return this.http.get(`${this.baseUrl}/usuario/${cpf}`)
    .toPromise()
    .then( response => {
      this.selecionados = response;
    });
  }

  listaDocumentoAnaliseCoordenador (): Promise<any> {
    return this.http.get(`${this.baseUrl}/analise-documento`)
      .toPromise()
      .then( response => response);
  }

  listaDocumentoParaCheckList (cpf: string): Promise<any> {
    return this.http.get(`${this.baseUrl}/checklist-documento/${cpf}`)
      .toPromise()
      .then( response => response);
  }

  listaDocumentoParaFinalizar (): Promise<any> {
    return this.http.get(`${this.baseUrl}/finalizar-documento`)
      .toPromise()
      .then( response => response);
  }

  listaDocumentoReceber (cpf: string): Promise<any> {
    return this.http.get(`${this.baseUrl}/funcionario/${cpf}`)
      .toPromise()
      .then( response => response);
  }

  listaInicioDistribuicao (): Promise<any> {
    return this.http.get(`${this.baseUrl}/inicio-distribuicao-documento`)
      .toPromise()
      .then( response => response);
  }

  distribuicaoEmBloco (): Promise<any> {
    return this.http.get(`${this.baseUrl}/distribuicao-bloco-documento`)
      .toPromise()
      .then( response => response);
  }

  calcularPrazoDias (dataEnvioRecebimento: Date, dataFimPrazo: Date): Promise<any> {
    let params = new HttpParams();
    params = params.append('dataEnvioRecebimento', moment(dataEnvioRecebimento).format('YYYY-MM-DD HH:mm:ss'));
    params = params.append('dataFimPrazo', moment(dataFimPrazo).format('YYYY-MM-DD HH:mm:ss'));
    return this.http.get<number>(`${this.baseUrl}/calcular-prazo`, {params})
      .toPromise()
      .then( response => response);
  }

  distribuicaoIndividual (id: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/distribuicao-individual-documento/${id}`)
      .toPromise()
      .then( response => response);
  }

  relatorioProcesso(id: number) {
    return this.http.get(`${this.baseUrl}/relatorioProcesso/${id}`,
      { responseType: 'blob' })
      .toPromise();
  }

  public converterStringsParaDatas(documentos: DocumentoJuridico[]) {
    for (const documento of documentos) {
      if (documento.dataDoc) {
        documento.dataDoc = moment(documento.dataDoc,
        'YYYY-MM-DD').toDate();
      }
      if (documento.dataConclusao) {
        documento.dataConclusao = moment(documento.dataConclusao,
          'YYYY-MM-DD').toDate();
      }
      if (documento.dataConclusaoReal) {
        documento.dataConclusaoReal = moment(documento.dataConclusaoReal,
          'YYYY-MM-DD').toDate();
      }
      if (documento.dataEnvioRecebimento) {
        documento.dataEnvioRecebimento = moment(documento.dataEnvioRecebimento,
          'YYYY-MM-DD').toDate();
      }
      if (documento.dataRecebimentoAdvogado) {
        documento.dataRecebimentoAdvogado = moment(documento.dataRecebimentoAdvogado,
          'YYYY-MM-DD').toDate();
      }
      if (documento.dataFimPrazo) {
        documento.dataFimPrazo = moment(documento.dataFimPrazo,
          'YYYY-MM-DD').toDate();
      }
    }
  }

}
