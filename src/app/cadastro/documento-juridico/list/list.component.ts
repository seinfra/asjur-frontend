import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MenuItem, LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { DocumentoJuridico } from '../documento-juridico';
import { DocumentoJuridicoFiltro, DocumentoJuridicoService } from '../documento-juridico.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public quantidadeItemPorPagina: any[];
  public quantidadeItemPorPaginaSelecionada: any;
  public selecionados: DocumentoJuridico[];
  public filtro = new DocumentoJuridicoFiltro;
  public totalRegistros = 0;
  public codigoEntidade: number;
  public consultar = true;
  public loading = false;
  public rotaNovo = "/asjur/documento-juridico/novo";
  public rotaPadrao = '/asjur/documento-juridico/';
  @ViewChild('tabela') tabelaEntidade;

  constructor(
    public entidadeService: DocumentoJuridicoService,
    public title: Title,
    public toastr: ToastrService,
    public errorHandler: ErrorHandlerService,
    public confirmation: ConfirmationService,
    public router: Router,
    public segurancaService: SegurancaService
  ) {}

  ngOnInit() {

    this.pesquisar();

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Listagem de Documento'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.quantidadeItemPorPagina = [
      {value:10},
      {value:20},
      {value:50},
    ]

    this.title.setTitle ('SeinfraSin - Documento');
  }

  pesquisar(pagina = 0) {
    this.loading = true;
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
        this.loading = false;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  editar (codigo: number) {
    this.router.navigate([this.rotaPadrao + codigo]);
  }

  excluir (codigo: number) {
      this.confirmation.confirm({
        message: 'Tem certeza que deseja excluir',
        accept: (() => {
            this.entidadeService.excluir(codigo)
              .then(() => {
                if(this.tabelaEntidade.first === 0){
                  this.pesquisar();
                }else {
                  this.tabelaEntidade.first = 0;
                }
                this.toastr.success('Item excluído com sucesso!');
              })
              .catch (erro => this.errorHandler.handle(erro));
        })
      });
  }

  limparFiltro () {
    this.filtro = new DocumentoJuridicoFiltro();
    this.pesquisar();
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  selecionarEntidade (entidade){
    this.codigoEntidade = entidade.id;
  }

}
