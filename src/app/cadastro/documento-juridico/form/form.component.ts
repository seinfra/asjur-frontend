import { Orgao } from 'src/app/cadastro/orgao/orgao';
import { FuncionarioService } from './../../../recursos-humanos/funcionario/funcionario.service';
import { SegurancaService } from './../../../seguranca/seguranca.service';
import { ArmarioService } from './../../armario/armario.service';
import { CaixaService } from './../../caixa/caixa.service';
import { PastaService } from './../../pasta/pasta.service';
import { ReferenciaService } from './../../referencia/referencia.service';
import { AssuntoService } from './../../assunto/assunto.service';
import { Assunto } from './../../assunto/assunto';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { TipoDocumentoService } from './../../tipo-documento/tipo-documento.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { TipoDocumento } from '../../tipo-documento/tipo-documento';
import { DocumentoJuridico } from '../documento-juridico';
import { DocumentoJuridicoService } from '../documento-juridico.service';
import { Armario } from '../../armario/armario';
import { Caixa } from '../../caixa/caixa';
import { Pasta } from '../../pasta/pasta';
import { Referencia } from '../../referencia/referencia';
import { Funcionario } from 'src/app/recursos-humanos/funcionario/funcionario';
import { OrgaoService } from '../../orgao/orgao.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public entidade = new DocumentoJuridico();
  public tipoDocumento = new TipoDocumento();
  public consulta: boolean;
  public codigoEntidade: number;
  public tiposDocumento: TipoDocumento[] = [];
  public assuntos: Assunto[] = [];
  public referencias: Referencia[] = [];
  public caixas: Caixa[] = [];
  public pastas: Pasta[] = [];
  public armarios: Armario[] = [];
  public orgaos: Orgao[] = [];
  public funcionarios: Funcionario[] = [];
  public temMovimentacao = false;
  public finalizaProcesso = false;
  public temPermissao = false;
  public prazo: any;
  public rotaListar = "/asjur/documento-juridico/listar";
  public pareceresJuridico = [
    { label: 'FAVORÁVEL', value: 'FAVORAVEL' },
    { label: 'DESFAVORÁVEL', value: 'DESFAVORÁVEL' },
    { label: 'OUTROS', value: 'OUTROS' },
  ]
  public ugbs = [
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  public prioridades = [
    { label: 'ALTISSÍMA', value: 'ALTISSIMA' },
    { label: 'ALTA', value: 'ALTA' },
    { label: 'MÉDIA', value: 'MEDIA' },
    { label: 'BAIXA', value: 'BAIXA' },
  ]

  //Campos para validação
  public nome = true;

  constructor(
    public entidadeService: DocumentoJuridicoService,
    public tipoDocumentoService: TipoDocumentoService,
    public assuntoService: AssuntoService,
    public referenciaService: ReferenciaService,
    public pastaService: PastaService,
    public caixaService: CaixaService,
    public armarioService: ArmarioService,
    public orgaoService: OrgaoService,
    public funcionarioService: FuncionarioService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public title: Title,
    public locale: DataLocaleService,
    public errorHandler : ErrorHandlerService,
    public segurancaService: SegurancaService
  ) { }

  ngOnInit() {

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Documento'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];
    this.listarTiposDocumento();
    this.listarAssuntos();
    this.listarReferencias();
    this.listarArmarios();
    this.listarCaixas();
    this.listarPastas();
    this.listarOrgaos();
    this.listarFuncionarios();

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }
  }

  incluir (form: FormControl) {
    this.entidadeService.incluirDocumento(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.buscar(this.codigoEntidade);

    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  listarTiposDocumento(){
    this.tipoDocumentoService.listar().then( element => {
      this.tiposDocumento = element.map(c => ({ label: c.descricao, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarAssuntos(){
    this.assuntoService.listar().then( element => {
      this.assuntos = element.map(c => ({ label: c.descricao, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarFuncionarios(){
    this.funcionarioService.funcionarioJuridico().then( element => {
      this.funcionarios = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarOrgaos(){
    this.orgaoService.listar().then( element => {
      this.orgaos = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarReferencias(){
    this.referenciaService.listar().then( element => {
      this.referencias = element.map(c => ({ label: c.descricao, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarArmarios(){
    this.armarioService.listar().then( element => {
      this.armarios = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarCaixas(){
    this.caixaService.listar().then( element => {
      this.caixas = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  listarPastas(){
    this.pastaService.listar().then( element => {
      this.pastas = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;
        this.entidadeService.calcularPrazoDias(this.entidade.dataEnvioRecebimento, this.entidade.dataFimPrazo)
        .then( element => {
          this.prazo = element;
        });
        if(this.segurancaService.jwtPayload.cpf !== null) {
          this.funcionarioService.funcionarioPorCpf(this.segurancaService.jwtPayload.cpf)
          .then(element => {
              if(element.cargo.nome !== 'ADVOGADO'){
                this.temPermissao = true;
              }
          })
        }

        if(this.entidade.movimentosDocumento.length > 0){
          this.temMovimentacao = true;
        }
        if(this.entidade.funcionario){
          if(this.segurancaService.jwtPayload.cpf === this.entidade.funcionario.cpf) {
            this.finalizaProcesso = true;
          }
        }

        if(!this.entidade.orgao){
          this.entidade.orgao = new Orgao();
        }
        if(!this.entidade.assunto){
          this.entidade.assunto = new Assunto();
        }
        if(!this.entidade.armario){
          this.entidade.armario = new Armario();
        }
        if(!this.entidade.caixa){
          this.entidade.caixa = new Caixa();
        }
        if(!this.entidade.pasta){
          this.entidade.pasta = new Pasta();
        }

        if(!this.entidade.funcionario){
          this.entidade.funcionario = new Funcionario();
        }
        this.entidadeService.converterStringsParaDatas([this.entidade]);

      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar (form: FormControl) {
    if (this.entidade.id){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  imprimir(codigo: number){
    this.entidadeService.relatorioProcesso(this.entidade.id)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
      });
  }

  limparFormulario (form: FormControl){
    this.entidade = new DocumentoJuridico();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }
  }

  finalizarProcesso(){
    this.entidade.dataConclusao = new Date();
    this.entidadeService.alterarDocumento(this.entidade);
    this.toastr.success('Processo finalizado com sucesso!');
  }

}
