import { Parecer } from './../../core/model/parecer';
import { Funcionario } from './../../recursos-humanos/funcionario/funcionario';
import { Pasta } from './../pasta/pasta';
import { Caixa } from './../caixa/caixa';
import { Assunto } from './../assunto/assunto';
import { Referencia } from './../referencia/referencia';
import { TipoDocumento } from './../tipo-documento/tipo-documento';
import { Armario } from '../armario/armario';
import { CheckList } from '../check-list/check-list';
import { Orgao } from '../orgao/orgao';
import { CheckListDocumentoPendente } from 'src/app/core/model/check-list-documento';
import { MovimentoDocumento } from 'src/app/core/model/movimentoDocumento';

export class DocumentoJuridico {
    id: number;
    categoriaDocumento: string;
    numeroDocumento: string;
    tipoDocumento = new TipoDocumento();
    referencia = new Referencia();
    numeroProcesso: string;
    interessado: string;
    demanda: string;
    dataDoc: Date;
    dataEnvioRecebimento: Date;
    dataRecebimentoAdvogado: Date;
    dataFimPrazo: Date;
    assunto = new Assunto();
    descricaoAssunto: string;
    detalhe: string;
    armario = new Armario();
    caixa = new Caixa();
    pasta = new Pasta();
    observacao: string;
    observacaoDiligencia: string;
    diligencia: string;
    dataConclusao: Date;
    dataConclusaoReal: Date;
    prazoLegal: boolean;
    ugb: string;
    redistribuicao: boolean;
    distribuir: boolean = false;
    prioridade: string;
    funcionario = new Funcionario();
    prazo: number;
    checkList = new CheckList();
    orgao = new Orgao();
    prevento:  boolean;
    status: string;
    parecerJuridico: string;
    parecerJuridicoDiligencia: string;
    subTipo: number;
    pareceres = new Array<Parecer>();
    documentosPendente = new Array<CheckListDocumentoPendente>();
    movimentosDocumento = new Array<MovimentoDocumento>();
  }




