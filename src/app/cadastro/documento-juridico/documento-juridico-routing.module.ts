import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/documento-juridico/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_JURIDICO_CADASTRAR']}
    },
    {
      path: 'asjur/documento-juridico/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_JURIDICO_CONSULTAR']}
    },
    {
      path: 'asjur/documento-juridico/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_JURIDICO_ALTERAR']}
    },
    {
      path: 'asjur/documento-juridico/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_JURIDICO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class DocumentoJuridicoRoutingModule { }
