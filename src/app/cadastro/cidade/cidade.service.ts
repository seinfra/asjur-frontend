import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { Cidade } from './cidade';
import { CrudService } from 'src/app/core/crud-service';

export class CidadeFiltro {
  id: number;
  nome: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class CidadeService extends CrudService<Cidade>{

  private baseUrl = `http://http://172.25.0.135:8080/administrador/administrador-api/cidade`;

  constructor(protected http: SistemaHttp) {
    super(http, `http://http://172.25.0.135:8080/administrador/administrador-api/cidade`);
  }

  pesquisar(filtro: CidadeFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

}
