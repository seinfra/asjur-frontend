import { Endereco } from 'src/app/core/model/endereco';

export class Orgao {
    id: number;
    nome: string;
    sigla: string;
    cnpj: string;
    tipo: string;
    endereco = new Endereco();

    constructor(id?: number){
      this.id = id;
    }
}
