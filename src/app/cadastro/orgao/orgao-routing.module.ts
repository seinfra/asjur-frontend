import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/orgao/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ORGAO_CADASTRAR']}
    },
    {
      path: 'asjur/orgao/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ORGAO_CONSULTAR']}
    },
    {
      path: 'asjur/orgao/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ORGAO_ALTERAR']}
    },
    {
      path: 'asjur/orgao/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['ORGAO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class OrgaoRoutingModule { }
