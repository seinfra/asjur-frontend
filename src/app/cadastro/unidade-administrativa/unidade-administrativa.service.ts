import { BuscarUnidadeAdministrativaComponent } from './../../componentes/buscar-unidade-administrativa/buscar-unidade-administrativa.component';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { UnidadeAdministrativa } from './unidade-administrativa';
import { CrudService } from 'src/app/core/crud-service';

export class UnidadeAdministrativaFiltro {
  id: number;
  nome: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class UnidadeAdministrativaService extends CrudService<UnidadeAdministrativa>{

  private baseUrl = `${environment.apiUrl}/asjur-api/unidade-administrativa-juridico`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/unidade-administrativa-juridico`);
  }

  pesquisar(filtro: UnidadeAdministrativaFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  buscarPorNome(filtro: string){
    let params = new HttpParams();
    params = params.append('nome', filtro);
    return this.http.get<any>(`${this.baseUrl}/buscarPorNome`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

}
