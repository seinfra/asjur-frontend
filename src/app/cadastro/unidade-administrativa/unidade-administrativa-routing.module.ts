import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/unidade-administrativa/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['UNIDADE_ADMINISTRATIVA_CADASTRAR']}
    },
    {
      path: 'asjur/unidade-administrativa/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['UNIDADE_ADMINISTRATIVA_CONSULTAR']}
    },
    {
      path: 'asjur/unidade-administrativa/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['UNIDADE_ADMINISTRATIVA_ALTERAR']}
    },
    {
      path: 'asjur/unidade-administrativa/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['UNIDADE_ADMINISTRATIVA_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class UnidadeAdministrativaRoutingModule { }
