export class UnidadeAdministrativa {
    id: number;
    nome: string;
    sigla: string;
    entidade: any;
    dataCadastro: Date;
    dataAlteracao: Date;
}
