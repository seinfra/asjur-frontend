import { CaixaService } from './../../caixa/caixa.service';
import { Caixa } from './../../caixa/caixa';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Pasta } from '../pasta';
import { PastaService } from '../pasta.service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public entidade = new Pasta();
  public consulta: boolean;
  public codigoEntidade: number;
  public rotaListar = "/asjur/pasta/listar";
  public caixas: Caixa[] = [];

  //Campos para validação
  public nome = true;

  constructor(
    public entidadeService: PastaService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public title: Title,
    public segurancaService: SegurancaService,
    public caixaService: CaixaService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Pasta'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];
    this.buscarCaixas();

    if(this.codigoEntidade){
      this.buscar(this.codigoEntidade);
    }
  }

  buscarCaixas(){
    this.caixaService.listar().then( element => {
      this.caixas = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  incluir (form: FormControl) {
    this.entidadeService.incluir(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar (form: FormControl) {
    this.entidadeService.alterar(this.entidade)
      .then( dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.entidade = dados;

        if(this.consulta){
          this.title.setTitle (`SeinfraSin - Consulta de Pasta:${this.entidade.nome}`);
        }else{
          this.title.setTitle (`SeinfraSin - Alteração de Pasta :${this.entidade.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number){}

  salvar (form: FormControl) {
    if (this.entidade.id){
      this.alterar(form);
    } else {
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Pasta();
    form.reset();
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }
  }

}
