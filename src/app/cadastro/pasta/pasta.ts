import { Caixa } from './../caixa/caixa';

export class Pasta {
    id: number;
    nome: string;
    ano: number;
    numero: number;
    caixa = new Caixa();
}
