import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/pasta/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PASTA_CADASTRAR']}
    },
    {
      path: 'asjur/pasta/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PASTA_CONSULTAR']}
    },
    {
      path: 'asjur/pasta/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PASTA_ALTERAR']}
    },
    {
      path: 'asjur/pasta/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['PASTA_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PastaRoutingModule { }
