import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/documento/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_CADASTRAR']}
    },
    {
      path: 'asjur/documento/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_CONSULTAR']}
    },
    {
      path: 'asjur/documento/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_ALTERAR']}
    },
    {
      path: 'asjur/documento/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['DOCUMENTO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class DocumentoRoutingModule { }
