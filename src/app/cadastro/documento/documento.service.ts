import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Documento } from './documento';
import * as moment from 'moment';

export class DocumentoFiltro {
  id: number;
  numeroDocumento: string;
  numeroProcesso: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class DocumentoService extends CrudService<Documento>{

  private baseUrl = `${environment.apiUrl}/asjur-api/documento-juridico`;

  private baseUrlEx = `${environment.apiUrlEx}/sad-api/documento`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/documento-juridico`)
  }

  pesquisar(filtro: DocumentoFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.numeroDocumento) {
      params = params.append('numeroDocumento', filtro.numeroDocumento);
    }

    if(filtro.numeroProcesso) {
      params = params.append('numeroProcesso', filtro.numeroProcesso);
    }

    return this.http.get<any>(this.baseUrlEx, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  buscarDocumento (codigo: number): Promise<any> {
    return this.http.get(`${this.baseUrlEx}/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  buscarDocumentoJuridico (codigo: number): Promise<any> {
    return this.http.get(`${this.baseUrl}/juridico/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  public converterStringsParaDatas(documentos: Documento[]) {
    for (const documento of documentos) {
      if (documento.dataDocumento) {
        documento.dataDocumento = moment(documento.dataDocumento,
        'YYYY-MM-DD').toDate();
      }
      if (documento.dataConclusao) {
        documento.dataConclusao = moment(documento.dataConclusao,
          'YYYY-MM-DD').toDate();
      }
      if (documento.dataConclusaoReal) {
        documento.dataConclusaoReal = moment(documento.dataConclusaoReal,
          'YYYY-MM-DD').toDate();
      }
    }
  }

  incluirDocumento(entidade: Documento): Promise<any> {
    return this.http.post<any>(`${this.baseUrl}/incluirDocumento`, entidade)
      .toPromise();
  }

}
