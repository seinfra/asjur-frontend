import { Assunto } from './../../assunto/assunto';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Documento } from '../documento';
import { DocumentoService } from '../documento.service';
import { TipoDocumento } from '../../tipo-documento/tipo-documento';
import { UnidadeAdministrativa } from '../../unidade-administrativa/unidade-administrativa';
import { Pasta } from '../../pasta/pasta';
import { Caixa } from '../../caixa/caixa';
import { Armario } from '../../armario/armario';
import { Referencia } from '../../referencia/referencia';
import { Orgao } from '../../orgao/orgao';
import { OrgaoService } from '../../orgao/orgao.service';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public entidade = new Documento();
  public documentoJuridico = new Documento();
  public tipoDocumento = new TipoDocumento();
  public unidadeAdministrativa = new UnidadeAdministrativa();
  public caixa = new Caixa();
  public pasta = new Pasta();
  public armario = new Armario();
  public referencia = new Referencia();
  public assunto = new Assunto();
  public orgaos: Orgao[] = [];
  public consulta: boolean;
  public codigoEntidade: number;

  public ugbs = [
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  public prioridades = [
    { label: 'ALTISSÍMA', value: 'ALTISSIMA' },
    { label: 'ALTA', value: 'ALTA' },
    { label: 'MÉDIA', value: 'MEDIA' },
    { label: 'BAIXA', value: 'BAIXA' },
  ]

  public rotaListar = "/asjur/documento/listar";

  //Campos para validação
  public nome = true;

  constructor(
    public entidadeService: DocumentoService,
    public orgaoService: OrgaoService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public title: Title,
    public locale: DataLocaleService,
    public segurancaService: SegurancaService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {

    this.items = [
      {label:'SeinfraSin'},
      {label:'Cadastro'},
      {label:'Documento'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};
    this.listarOrgaos();

    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];
    if(this.codigoEntidade){
      this.buscarDocumento(this.codigoEntidade);
    }

  }

  incluir (form: FormControl) {
    this.entidadeService.incluirDocumento(this.entidade)
      .then( ()=>{
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  buscarDocumento (codigo: number) {
    this.entidadeService.buscarDocumento(codigo)
      .then( dados => {
        this.entidade = dados;

        if(!this.entidade.orgao){
          this.entidade.orgao = new Orgao();
        }
        if(!this.entidade.armario){
          this.entidade.armario = new Armario();
        }
        if(!this.entidade.caixa){
          this.entidade.caixa = new Caixa();
        }
        if(!this.entidade.pasta){
          this.entidade.pasta = new Pasta();
        }
        if(!this.entidade.assunto){
          this.entidade.assunto = new Assunto();
        }
        if(!this.entidade.referencia){
          this.entidade.referencia = new Referencia();
        }
        this.entidadeService.converterStringsParaDatas([this.entidade]);

        if(this.consulta){
          this.title.setTitle (`SeinfraSin - Consulta de Documento:${this.entidade.numeroDocumento}`);
        }else{
          this.title.setTitle (`SeinfraSin - Alteração de Documento :${this.entidade.numeroDocumento}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  buscar (codigo: number) {
    this.entidadeService.buscar(codigo)
      .then( dados => {
        this.documentoJuridico = dados;
        this.entidadeService.converterStringsParaDatas([this.documentoJuridico]);

      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number){}

  salvar (form: FormControl) {
    if(this.entidade.ugb === 'COMPLIANCE' && this.entidade.orgao.id === undefined){
      this.toastr.error('Campo orgão é obrigatório para UGB compliance!');
    }else{
      this.incluir(form);
    }
  }

  limparFormulario (form: FormControl){
    this.entidade = new Documento();
    form.reset();
  }

  listarOrgaos(){
    this.orgaoService.listar().then( element => {
      this.orgaos = element.map(c => ({ label: c.nome, value: c.id }));
    })
    .catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

  exibiErroFormulario(form: FormControl){

    if(!form.value.nome) {
      this.nome = false;
    }else {
      this.nome = true;
    }

  }

}
