import { Parecer } from './../../core/model/parecer';
import { Orgao } from './../orgao/orgao';
import { Pasta } from './../pasta/pasta';
import { Caixa } from './../caixa/caixa';
import { UnidadeAdministrativa } from 'src/app/cadastro/unidade-administrativa/unidade-administrativa';
import { Assunto } from './../assunto/assunto';
import { Referencia } from './../referencia/referencia';
import { TipoDocumento } from './../tipo-documento/tipo-documento';
import { Armario } from '../armario/armario';

export class Documento {
    id: number;
    categoriaDocumento: string;
    numeroDocumento: string;
    tipoDocumento = new TipoDocumento();
    referencia = new Referencia();
    numeroProcesso: string;
    interessado: string;
    demanda: string;
    demandaAtendida: string;
    valorDemanda: number;
    dataDocumento: Date;
    dataEnvioRecebimento: Date;
    assunto = new Assunto();
    descricaoAssunto: string;
    detalhe: string;
    unidadeAdministrativa = new UnidadeAdministrativa();
    armario = new Armario();
    caixa = new Caixa();
    pasta = new Pasta();
    observacao: string;
    urlArquivo: string;
    dataConclusao: Date;
    dataConclusaoReal: Date;
    prazoLegal: boolean;
    arquivar: boolean;
    prioridade: string;
    prazo: number;
    ugb: string;
    pareceres = new Array<Parecer>();
    orgao = new Orgao();
}




