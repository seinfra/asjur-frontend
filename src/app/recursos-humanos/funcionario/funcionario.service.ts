import { Funcionario } from './funcionario';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import * as moment from 'moment';
import { PlanoFerias } from 'src/app/core/model/planoFerias';
import { Requerimento } from 'src/app/core/model/requerimento';

export class FuncionarioFiltro {
  id: number;
  nome: string;
  numero: number
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})
export class FuncionarioService extends CrudService<Funcionario>{

  private baseUrl = `${environment.apiUrl}/asjur-api/funcionario`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/funcionario`);
  }

  pesquisar(filtro: FuncionarioFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

  incluirFuncionario(entidade: Funcionario): Promise<any> {
    return this.http.post<any>(`${this.baseUrl}/incluir-funcionario`, entidade)
      .toPromise();
  }

  alterarFuncionario(entidade: Funcionario): Promise<any> {
    return this.http.put<any>(`${this.baseUrl}/alterar-funcionario/${entidade.id}`, entidade)
      .toPromise();
  }

  funcionarioPorCpf(cpf: string): Promise<any> {
    return this.http.get(`${this.baseUrl}/funcionario/${cpf}`)
      .toPromise()
      .then( response => response);
  }

  funcionarioJuridico(): Promise<any> {
    return this.http.get(`${this.baseUrl}/funcionario-juridico`)
      .toPromise()
      .then( response => response);
  }

  calcularDiaFerias(dataEntrada: Date, dataFim: Date){
    let params = new HttpParams();
    params = params.append('dataEntrada', moment(dataEntrada).format('YYYY-MM-DD'));
    params = params.append('dataFim', moment(dataFim).format('YYYY-MM-DD'));
    return this.http.get<number>(`${this.baseUrl}/calcularDiaFerias`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

  calcularDiaSaldoFerias(dataEntrada: Date, dataFim: Date, dataEntradaSaldo: Date, dataFimSaldo: Date){
    let params = new HttpParams();
    params = params.append('dataEntrada', moment(dataEntrada).format('YYYY-MM-DD'));
    params = params.append('dataFim', moment(dataFim).format('YYYY-MM-DD'));
    params = params.append('dataEntradaSaldo', moment(dataEntradaSaldo).format('YYYY-MM-DD'));
    params = params.append('dataFimSaldo', moment(dataFimSaldo).format('YYYY-MM-DD'));
    return this.http.get<number>(`${this.baseUrl}/calcularDiaSaldoFerias`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

  calcularDiaRestanteFerias(dataEntrada: Date, dataFim: Date, dataEntradaSaldo: Date, dataFimSaldo: Date, dataEntradaSaldoRestante: Date, dataFimSaldoRestante: Date){
    let params = new HttpParams();
    params = params.append('dataEntrada', moment(dataEntrada).format('YYYY-MM-DD'));
    params = params.append('dataFim', moment(dataFim).format('YYYY-MM-DD'));
    params = params.append('dataEntradaSaldo', moment(dataEntradaSaldo).format('YYYY-MM-DD'));
    params = params.append('dataFimSaldo', moment(dataFimSaldo).format('YYYY-MM-DD'));
    params = params.append('dataEntradaSaldoRestante', moment(dataEntradaSaldoRestante).format('YYYY-MM-DD'));
    params = params.append('dataFimSaldoRestante', moment(dataFimSaldoRestante).format('YYYY-MM-DD'));
    return this.http.get<number>(`${this.baseUrl}/calcularDiaRestanteFerias`, {params})
    .toPromise()
    .then( response => {
      return response
    });
  }

  public converterStringsParaDatas(funcionarios: Funcionario[]) {
    for (const funcionario of funcionarios) {
      if (funcionario.dataNascimento) {
      funcionario.dataNascimento = moment(funcionario.dataNascimento,
        'YYYY-MM-DD').toDate();
      }
      if (funcionario.dataEntrada) {
        funcionario.dataEntrada = moment(funcionario.dataEntrada,
          'YYYY-MM-DD').toDate();
      }
      if (funcionario.dataSaida) {
        funcionario.dataSaida = moment(funcionario.dataSaida,
          'YYYY-MM-DD').toDate();
      }
    }
  }

  public converterStringsParaDatasRequerimento(requerimentos: Requerimento[]) {
    for (const requerimento of requerimentos) {
      if (requerimento.dataInicial) {
        requerimento.dataInicial = moment(requerimento.dataInicial,
        'YYYY-MM-DD').toDate();
      }
      if (requerimento.dataFinal) {
        requerimento.dataFinal = moment(requerimento.dataFinal,
          'YYYY-MM-DD').toDate();
      }
    }
  }

  public converterStringsParaDatasFerias(planosFerias: PlanoFerias[]) {
    for (const planoFerias of planosFerias) {
      planoFerias.dataInicial = moment(planoFerias.dataInicial,
        'YYYY-MM-DD').toDate();

      if (planoFerias.dataFinal) {
        planoFerias.dataFinal = moment(planoFerias.dataFinal,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataInicialSaldo) {
        planoFerias.dataInicialSaldo = moment(planoFerias.dataInicialSaldo,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataFinalSaldo) {
        planoFerias.dataFinalSaldo = moment(planoFerias.dataFinalSaldo,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataSolicitacao) {
        planoFerias.dataSolicitacao = moment(planoFerias.dataSolicitacao,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataSolicitacaoSaldo) {
        planoFerias.dataSolicitacaoSaldo = moment(planoFerias.dataSolicitacaoSaldo,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataInicialSaldoRestante) {
        planoFerias.dataInicialSaldoRestante = moment(planoFerias.dataInicialSaldoRestante,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataFinalSaldoRestante) {
        planoFerias.dataFinalSaldoRestante = moment(planoFerias.dataFinalSaldoRestante,
          'YYYY-MM-DD').toDate();
      }
      if (planoFerias.dataSolicitacaoSaldoRestante) {
        planoFerias.dataSolicitacaoSaldoRestante = moment(planoFerias.dataSolicitacaoSaldoRestante,
          'YYYY-MM-DD').toDate();
      }
    }
  }
}
