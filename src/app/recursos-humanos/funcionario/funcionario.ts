import { ContatoPessoal } from './../../core/model/contatoPessoal';
import { Credor } from 'src/app/core/model/credor';
import { Orgao } from './../../cadastro/orgao/orgao';
import { Usuario } from './../../seguranca/usuario/usuario';
import { Cargo } from './../cargo/cargo';
import { Endereco } from './../../core/model/endereco';
import { Contato } from './../../core/model/contato';
import { PlanoFerias } from 'src/app/core/model/planoFerias';
import { Requerimento } from 'src/app/core/model/requerimento';

export class Funcionario {
  id: number;
  nome: string;
  dataNascimento: Date;
  dataEntrada: Date;
  dataSaida: Date;
  cpf: string;
  rg: string;
  ugb: any;
  contatos = new Array<Contato>();
  contatosPessoais = new Array<ContatoPessoal>();
  planoFerias = new Array<PlanoFerias>();
  requerimentos = new Array<Requerimento>();
  endereco = new Endereco();
  orgaosFuncionario = new Array<OrgaoFuncionario>();
  orgaos = new Array<Orgao>();
  credoresFuncionario = new Array<CredorFuncionario>();
  credores = new Array<Credor>();
  cargo = new Cargo();
  usuario = new Usuario();
  status: string;
  matricula: string;

  constructor(id?: number, nome?: string){
    this.id = id;
    this.nome = nome;
  }
}

export class OrgaoFuncionario {
  id: number;
  funcionario = new Funcionario();
  orgao = new Orgao();

    constructor(orgao?: Orgao, funcionario?: Funcionario){
      this.orgao = orgao
      this.funcionario = funcionario;
  }
}

export class CredorFuncionario {
  id: number;
  funcionario = new Funcionario();
  credor = new Credor();

    constructor(credor?: Credor, funcionario?: Funcionario){
      this.credor = credor
      this.funcionario = funcionario;
    }
  }

