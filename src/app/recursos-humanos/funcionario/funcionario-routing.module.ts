
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegurancaGuard } from 'src/app/seguranca/seguranca.guard';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
      path: 'asjur/funcionario/novo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FUNCIONARIO_CADASTRAR']}
    },
    {
      path: 'asjur/funcionario/listar',
      component: ListComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FUNCIONARIO_CONSULTAR']}
    },
    {
      path: 'asjur/funcionario/:codigo',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FUNCIONARIO_ALTERAR']}
    },
    {
      path: 'asjur/funcionario/:codigo/:consulta',
      component: FormComponent,
      canActivate:[SegurancaGuard],
      data: { roles: ['FUNCIONARIO_CONSULTAR']}
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FuncionarioRoutingModule { }
