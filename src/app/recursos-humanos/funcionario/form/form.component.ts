import { ContatoPessoal } from './../../../core/model/contatoPessoal';
import { CredorFuncionarioService, CredorFuncionarioFiltro } from './../../../core/service/credor-funcionario.service';
import { OrgaoFuncionarioFiltro } from './../../../core/service/orgao-funcionario.service';
import { Credor } from './../../../core/model/credor';
import { Municipio } from './../../../core/model/municipio';
import { AssuntoService } from './../../../cadastro/assunto/assunto.service';
import { UsuarioService } from './../../../seguranca/usuario/usuario.service';
import { PlanoFerias } from './../../../core/model/planoFerias';
import { CargoService } from './../../cargo/cargo.service';
import { FuncionarioService } from './../funcionario.service';
import { Funcionario, OrgaoFuncionario, CredorFuncionario } from './../funcionario';
import { Contato } from './../../../core/model/contato';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem, ConfirmationService } from 'primeng/components/common/api';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Endereco } from 'src/app/core/model/endereco';
import { EstadoEnum } from 'src/app/core/enum/estado-enum';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { Cargo } from '../../cargo/cargo';
import { OrgaoService } from 'src/app/cadastro/orgao/orgao.service';
import { Usuario } from 'src/app/seguranca/usuario/usuario';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { Orgao } from 'src/app/cadastro/orgao/orgao';
import { Requerimento } from 'src/app/core/model/requerimento';
import { Assunto } from 'src/app/cadastro/assunto/assunto';
import { CredorService } from 'src/app/core/service/credor.service';
import { MunicipioService } from 'src/app/core/service/municipio.service';
import { OrgaoFuncionarioService } from 'src/app/core/service/orgao-funcionario.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public items: MenuItem[];
  public entidade = new Funcionario();
  public consulta: boolean;
  public codigoEntidade: number;
  public rotaListar = "/asjur/funcionario/listar";
  public contatosSelecionados: Contato[] = [];
  public contatosPessoalSelecionados: ContatoPessoal[] = [];
  public planoFeriasSelecionados: PlanoFerias[] = [];
  public requerimentoSelecionados: Requerimento[] = [];
  public contato = new Contato();
  public contatoPessoal = new ContatoPessoal();
  public planoFerias = new PlanoFerias();
  public requerimento = new Requerimento();
  public visibleDialogContato: boolean;
  public visibleDialogContatoPessoal: boolean;
  public visibleDialogFerias: boolean;
  public visibleDialogRequerimento: boolean;
  public indexContato: number;
  public indexContatoPessoal: number;
  public indexPlanoFerias: number;
  public indexRequerimento: number;
  public editandoContato: boolean;
  public editandoContatoPessoal: boolean;
  public editandoPlanoFerias: boolean;
  public editandoRequerimento: boolean;
  public valorTotalPedido = 0;
  public homeBreadcrumb: MenuItem;
  public temSaldoRestante = false;
  public estados = new EstadoEnum();
  public municipios: Municipio[] = [];
  public credores: Credor[] = [];
  public cargos: Cargo[] = [];
  public orgaos: Orgao[] = [];
  public assuntos: Assunto[] = [];
  public usuarios: Usuario[] = [];
  public orgaosSelecionados: Orgao[] = [];
  public credoresSelecionados: Credor[] = [];
  public qtdSaldo = 0;
  public qtdSaldoRestante = 0;

  public ugbs = [
    { label: 'APOIO', value: 'APOIO' },
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'COORDENAÇÃO', value: 'COORDENACAO' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  public statusFerias = [
    { label: 'Á SER GOZADA', value: 'A SER GOZADA' },
    { label: 'EXPIRADA', value: 'EXPIRADA' },
    { label: 'GOZADA', value: 'GOZADA' },
    { label: 'VENDIDA', value: 'VENDIDA' },
  ]

  public statusDados = [
    { label: 'ATIVO', value: 'ATIVO' },
    { label: 'INATIVO', value: 'INATIVO' },
    { label: 'AFASTADO', value: 'AFASTADO' },
    { label: 'LICENÇA MÉDICA', value: 'LICENCA MEDICA' },
  ]

  //Campos para validação
  public nome = true;
  public razaoSocial = true;
  public tipo = true;
  public status = true;
  public cpfCnpj = true;
  public endereco = true;
  public email = true;
  public telefone = true;
  public valor = true;
  public dataPedido = true;
  public dataVencimento = true;

  constructor(
    public entidadeService: FuncionarioService,
    public route: ActivatedRoute,
    public router: Router,
    public toastr: ToastrService,
    public confirmation: ConfirmationService,
    public title: Title,
    public segurancaService: SegurancaService,
    public errorHandler: ErrorHandlerService,
    public locale: DataLocaleService,
    public cargoService: CargoService,
    public orgaoService: OrgaoService,
    public usuarioService: UsuarioService,
    public assuntoService: AssuntoService,
    public credorService: CredorService,
    public municipioService: MunicipioService,
    public orgaoFuncionarioService: OrgaoFuncionarioService,
    public credorFuncionarioService: CredorFuncionarioService,
  ) { }

  ngOnInit() {
    this.items = [
      { label: 'SeinfraSin' },
      { label: 'Cadastro' },
      { label: 'Funcionário' }
    ];

    this.homeBreadcrumb = { icon: 'fa fa-home' };
    this.buscarCargos();
    this.buscarOrgaos();
    this.buscarUsuarios();
    this.buscarAssuntos();
    this.buscarMunicipios();
    this.locale;
    this.valorTotalPedido;
    this.temSaldoRestante = false;
    this.codigoEntidade = this.route.snapshot.params['codigo'];
    this.consulta = this.route.snapshot.params['consulta'];

    if (!this.requerimento.assunto) {
      this.requerimento.assunto = new Assunto();
    }

    if (this.codigoEntidade) {
      this.buscar(this.codigoEntidade);
    }
  }

  buscarAssuntos() {
    this.assuntoService.listar().then(element => {
      this.assuntos = element.map(c => ({ label: c.descricao, value: c.id }));
    })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  buscarMunicipios() {
    this.municipioService.listar().then(element => {
      this.municipios = element.map(c => ({ label: c.nome, value: c.id }));
    })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  buscarUsuarios() {
    this.usuarioService.listar().then(element => {
      this.usuarios = element.map(c => ({ label: c.email, value: c.id }));
    })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  buscarCargos() {
    this.cargoService.listar().then(element => {
      this.cargos = element.map(c => ({ label: c.nome, value: c.id }));
    })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  buscarOrgaos() {
    this.orgaoService.listar().then(element => {
      this.orgaos = element.map(c => ({ label: c.nome, value: c.id }));
    })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  incluir(form: FormControl) {
    this.entidadeService.incluirFuncionario(this.entidade)
      .then(() => {
        this.toastr.success('Cadastro realizado com sucesso');
        this.limparFormulario(form);
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.exibiErroFormulario(form);
      });
  }

  alterar(form: FormControl) {
    this.entidadeService.alterarFuncionario(this.entidade)
      .then(dados => {
        this.entidade = dados;
        this.toastr.success('Alteração realizada com sucesso');
        this.router.navigate([this.rotaListar]);
      })
      .catch(erro => {
        this.errorHandler.handle(erro)
        this.exibiErroFormulario(form);
      });
  }

  buscar(codigo: number) {
    this.entidadeService.buscar(codigo)
      .then(dados => {
        this.entidade = dados;

        this.entidadeService.converterStringsParaDatasRequerimento(this.entidade.requerimentos);
        this.entidadeService.converterStringsParaDatasFerias(this.entidade.planoFerias);
        this.entidadeService.converterStringsParaDatas([this.entidade]);
        if (!this.entidade.endereco) {
          this.entidade.endereco = new Endereco();
        }
        if (!this.entidade.endereco.municipio) {
          this.entidade.endereco.municipio = new Municipio();
        }

        this.preencherContatosSalvo();
        this.preencherContatoPessoalsSalvo();
        this.preencherPlanoFeriasSalvo();
        this.preencherOrgaoFuncionarioSalvo();
        this.preencherRequerimentoSalvo();
        this.preencherCredorFuncionarioSalvo();


        if (this.consulta) {
          this.title.setTitle(`SeinfraSin - Consulta de Fucionário:${this.entidade.nome}`);
        } else {
          this.title.setTitle(`SeinfraSin - Alteração de Funcionário :${this.entidade.nome}`);
        }
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  imprimir(codigo: number) { }

  salvar(form: FormControl) {
    if (!this.entidade.cargo.id) {
      this.toastr.error('Cargo é obrigatório(a), Ocorreu um erro ao processar a solicitação.');
    } else {
      this.preencherContatosParaSalvar();
      this.preencherContatosPessoalParaSalvar();
      this.preencherPlanoFeriasParaSalvar();
      this.preencheOrgaoParaSalvar();
      this.preencherRequerimentoParaSalvar();
      this.preencheCredorFuncionarioParaSalvar();

      if (this.entidade.id) {
        this.alterar(form);
      } else {
        this.incluir(form);
      }
    }
  }

  limparFormulario(form: FormControl) {
    this.entidade = new Funcionario();
    form.reset();
  }

  exibiErroFormulario(form: FormControl) {
  }

  //Contato
  //----------------------------------------------------------------------------
  salvarContato(contato: Contato) {
    if (contato.email || contato.telefone || contato.ramal) {

      if (this.editandoContato) {
        this.contatosSelecionados[this.indexContato] = new Contato(contato.codigo, contato.email, contato.telefone, contato.ramal);
      } else {
        this.contatosSelecionados.push(contato);
      }
      this.contato = new Contato();
      this.visibleDialogContato = false;
      this.editandoContato = false;

    } else {
      this.toastr.error('Informe o telefone ou e-mail!');
      this.email = false;
      this.telefone = false;
    }
  }

  editarContato(contato: Contato, index: number) {
    this.contato = new Contato(contato.codigo, contato.email, contato.telefone, contato.ramal);
    this.indexContato = index;
    this.editandoContato = true
    this.visibleDialogContato = true;
  }

  removerContato(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.contatosSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  preencherContatosParaSalvar() {
    this.entidade.contatos = new Array<Contato>();

    for (let i = 0; i < this.contatosSelecionados.length; i++) {
      let contato: Contato = this.contatosSelecionados[i];
      this.entidade.contatos.push(new Contato(contato.codigo, contato.email, contato.telefone, contato.ramal));
    }
  }

  preencherContatosSalvo() {
    for (let i = 0; i < this.entidade.contatos.length; i++) {
      let contato = this.entidade.contatos[i];
      this.contatosSelecionados.push(contato);
    }
  }

  showDialogContato() {
    this.visibleDialogContato = true;
  }

  cancelarModal() {
    this.visibleDialogContato = false;
  }

    //Contato Pessoal
  //----------------------------------------------------------------------------
  salvarContatoPessoal(contatoPessoal: ContatoPessoal) {
    if (contatoPessoal.email || contatoPessoal.telefone || contatoPessoal.celular) {

      if (this.editandoContatoPessoal) {
        this.contatosPessoalSelecionados[this.indexContatoPessoal] = new ContatoPessoal(contatoPessoal.codigo, contatoPessoal.email, contatoPessoal.telefone, contatoPessoal.celular);
      } else {
        this.contatosPessoalSelecionados.push(contatoPessoal);
      }
      this.contatoPessoal = new ContatoPessoal();
      this.visibleDialogContatoPessoal = false;
      this.editandoContatoPessoal = false;

    } else {
      this.toastr.error('Informe o telefone ou e-mail!');
      this.email = false;
      this.telefone = false;
    }
  }

  editarContatoPessoal(contatoPessoal: ContatoPessoal, index: number) {
    this.contatoPessoal = new ContatoPessoal(contatoPessoal.codigo, contatoPessoal.email, contatoPessoal.telefone, contatoPessoal.celular);
    this.indexContatoPessoal = index;
    this.editandoContatoPessoal = true
    this.visibleDialogContatoPessoal = true;
  }

  removerContatoPessoal(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.contatosPessoalSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  preencherContatosPessoalParaSalvar() {
    this.entidade.contatosPessoais = new Array<ContatoPessoal>();

    for (let i = 0; i < this.contatosPessoalSelecionados.length; i++) {
      let contatoPessoal: ContatoPessoal = this.contatosPessoalSelecionados[i];
      this.entidade.contatosPessoais.push(new ContatoPessoal(contatoPessoal.codigo, contatoPessoal.email, contatoPessoal.telefone, contatoPessoal.celular));
    }
  }

  preencherContatoPessoalsSalvo() {
    for (let i = 0; i < this.entidade.contatosPessoais.length; i++) {
      let contatoPessoal = this.entidade.contatosPessoais[i];
      this.contatosPessoalSelecionados.push(contatoPessoal);
    }
  }

  showDialogContatoPessoal() {
    this.visibleDialogContatoPessoal = true;
  }

  cancelarModalContatoPessoal() {
    this.visibleDialogContatoPessoal = false;
  }

  //Plano Férias
  //---------------------------------------------------------------------
  salvaPlanoFerias(planoFerias: PlanoFerias) {

    if (planoFerias.ano || planoFerias.dataInicial || planoFerias.dataFinal ||
      planoFerias.quantidadeDias || planoFerias.dataInicialSaldo || planoFerias.dataFinalSaldo ||
      planoFerias.quantidadeDiasSaldo || planoFerias.observacao || planoFerias.statusFerias) {

      if (this.editandoPlanoFerias) {
        this.planoFeriasSelecionados[this.indexPlanoFerias] = new PlanoFerias(
          planoFerias.ano, planoFerias.dataInicial,
          planoFerias.dataFinal, planoFerias.quantidadeDias,
          planoFerias.dataInicialSaldo, planoFerias.dataFinalSaldo,
          planoFerias.quantidadeDiasSaldo, planoFerias.observacao,
          planoFerias.statusFerias, planoFerias.dataSolicitacao,
          planoFerias.dataSolicitacaoSaldo, planoFerias.observacaoSaldo,
          planoFerias.dataSolicitacaoSaldoRestante, planoFerias.dataInicialSaldoRestante,
          planoFerias.dataFinalSaldoRestante, planoFerias.quantidadeDiasSaldoRestante,
          planoFerias.observacaoSaldoRestante, planoFerias.statusFeriasSaldo, planoFerias.statusFeriasSaldoRestante);
      } else {
        this.planoFeriasSelecionados.push(planoFerias);
      }
      this.planoFerias = new PlanoFerias();
      this.visibleDialogFerias = false;
      this.editandoPlanoFerias = false;

    } else {
      this.toastr.error('Informe os dados de plano de férias!');
      this.email = false;
      this.telefone = false;
    }
  }

  editarPlanoFerias(planoFerias: PlanoFerias, index: number) {
    this.planoFerias = new PlanoFerias(
      planoFerias.ano, planoFerias.dataInicial,
      planoFerias.dataFinal, planoFerias.quantidadeDias,
      planoFerias.dataInicialSaldo, planoFerias.dataFinalSaldo,
      planoFerias.quantidadeDiasSaldo, planoFerias.observacao,
      planoFerias.statusFerias, planoFerias.dataSolicitacao,
      planoFerias.dataSolicitacaoSaldo, planoFerias.observacaoSaldo,
      planoFerias.dataSolicitacaoSaldoRestante, planoFerias.dataInicialSaldoRestante,
      planoFerias.dataFinalSaldoRestante, planoFerias.quantidadeDiasSaldoRestante,
      planoFerias.observacaoSaldoRestante, planoFerias.statusFeriasSaldo, planoFerias.statusFeriasSaldoRestante);

      if (this.planoFerias.quantidadeDiasSaldoRestante) {
          this.temSaldoRestante = true;
      }
    this.indexPlanoFerias = index;
    this.editandoPlanoFerias = true
    this.visibleDialogFerias = true;
  }

  removerPlanoFerias(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.planoFeriasSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  preencherPlanoFeriasParaSalvar() {
    this.entidade.planoFerias = new Array<PlanoFerias>();

    for (let i = 0; i < this.planoFeriasSelecionados.length; i++) {
      let planoFerias: PlanoFerias = this.planoFeriasSelecionados[i];
      this.entidade.planoFerias.push(new PlanoFerias(
        planoFerias.ano, planoFerias.dataInicial,
        planoFerias.dataFinal, planoFerias.quantidadeDias,
        planoFerias.dataInicialSaldo, planoFerias.dataFinalSaldo,
        planoFerias.quantidadeDiasSaldo, planoFerias.observacao,
        planoFerias.statusFerias, planoFerias.dataSolicitacao,
        planoFerias.dataSolicitacaoSaldo, planoFerias.observacaoSaldo,
        planoFerias.dataSolicitacaoSaldoRestante, planoFerias.dataInicialSaldoRestante,
        planoFerias.dataFinalSaldoRestante, planoFerias.quantidadeDiasSaldoRestante,
        planoFerias.observacaoSaldoRestante, planoFerias.statusFeriasSaldo, planoFerias.statusFeriasSaldoRestante));
    }
  }

  preencherPlanoFeriasSalvo() {
    for (let i = 0; i < this.entidade.planoFerias.length; i++) {
      let planoFerias = this.entidade.planoFerias[i];
      this.planoFeriasSelecionados.push(planoFerias);
    }
  }

  showDialogFerias() {
    this.planoFerias.ano = 2020;
    this.planoFerias.dataSolicitacao = new Date();
    this.visibleDialogFerias = true;
  }

  cancelarModalFerias() {
    this.visibleDialogFerias = false;
  }

  calcularDiaFerias(dataEntrada: Date, dataFim: Date) {
    if (dataEntrada != null && dataFim != null) {
      this.entidadeService.calcularDiaFerias(dataEntrada, dataFim).then(res => {
        this.planoFerias.quantidadeDias = res;
        this.qtdSaldo = 30 - this.planoFerias.quantidadeDias;
      });
    }
  }

  calcularDiaSaldoFerias(dataEntrada: Date, dataFim: Date) {
    if (dataEntrada != null && dataFim != null) {
      this.entidadeService.calcularDiaFerias(dataEntrada, dataFim).then(res => {
        this.planoFerias.quantidadeDiasSaldo = res;
        if(res > 0) {
          this.temSaldoRestante = true;
          this.entidadeService.calcularDiaFerias(dataEntrada, dataFim).then(res => {
            this.planoFerias.quantidadeDiasSaldo = res;
          });
          this.entidadeService.calcularDiaSaldoFerias(this.planoFerias.dataInicial, this.planoFerias.dataFinal, dataEntrada, dataFim).then(res => {
            this.qtdSaldo = res - 1;
            this.qtdSaldoRestante = this.qtdSaldo;
          });
        }
      });
    }
  }

  calcularDiaSaldoRestanteFerias(dataEntrada: Date, dataFim: Date) {
    if (dataEntrada != null && dataFim != null) {
      this.entidadeService.calcularDiaFerias(dataEntrada, dataFim).then(res => {
        this.planoFerias.quantidadeDiasSaldoRestante = res;
      });
      this.entidadeService.calcularDiaRestanteFerias(this.planoFerias.dataInicial, this.planoFerias.dataFinal, this.planoFerias.dataInicialSaldo, this.planoFerias.dataFinalSaldo, dataEntrada, dataFim).then(res => {
        this.qtdSaldoRestante = res;
      });
    }
  }

  //Requerimento
  //----------------------------------------------------------------------------------------------

  salvaRequerimento(requerimento: Requerimento) {
    this.assuntoService.buscar(this.requerimento.assunto.id).then(element => {
      requerimento.assunto = element;
    });

    if (requerimento.dataInicial || requerimento.dataFinal || requerimento.observacao) {

      if (this.editandoRequerimento) {
        this.requerimentoSelecionados[this.indexRequerimento] = new Requerimento(
          requerimento.dataInicial,
          requerimento.dataFinal,
          requerimento.observacao,
          requerimento.funcionario,
          requerimento.assunto);
      } else {
        this.requerimentoSelecionados.push(requerimento);
      }
      this.requerimento = new Requerimento();
      this.requerimento.assunto = new Assunto();
      this.visibleDialogRequerimento = false;
      this.editandoRequerimento = false;

    } else {
      this.toastr.error('Informe os dados requerimento!');
    }
  }

  editarRequerimento(requerimento: Requerimento, index: number) {
    this.requerimento = new Requerimento(
      requerimento.dataInicial,
      requerimento.dataFinal,
      requerimento.observacao,
      requerimento.funcionario,
      requerimento.assunto);
    this.indexRequerimento = index;
    this.editandoRequerimento = true
    this.visibleDialogRequerimento = true;
  }

  removerRequerimento(index: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.requerimentoSelecionados.splice(index, 1);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  preencherRequerimentoParaSalvar() {
    this.entidade.requerimentos = new Array<Requerimento>();

    for (let i = 0; i < this.requerimentoSelecionados.length; i++) {
      let requerimento: Requerimento = this.requerimentoSelecionados[i];
      this.entidade.requerimentos.push(new Requerimento(
        requerimento.dataInicial,
        requerimento.dataFinal,
        requerimento.observacao,
        requerimento.funcionario,
        requerimento.assunto));
    }
  }

  preencherRequerimentoSalvo() {
    for (let i = 0; i < this.entidade.requerimentos.length; i++) {
      let requerimento = this.entidade.requerimentos[i];
      this.requerimentoSelecionados.push(requerimento);
    }
  }

  showDialogRequerimento() {
    this.visibleDialogRequerimento = true;
  }

  cancelarModalRequerimento() {
    this.visibleDialogRequerimento = false;
  }

  //Orgão
  //----------------------------------------------------------------------------------------------

  preencheOrgaoParaSalvar() {
    if (this.orgaosSelecionados.length > 0) {
      this.entidade.orgaos = new Array<Orgao>();
      for (let i = 0; i < this.orgaosSelecionados.length; i++) {
        let orgao: any = this.orgaosSelecionados[i];
        this.entidade.orgaos.push(new Orgao(orgao.id));
      }
    }
  }

  preencherOrgaoSalvo() {
    if (this.entidade.orgaosFuncionario.length > 0) {
      this.entidade.orgaosFuncionario.forEach(element => {
        this.orgaosSelecionados.push(element.orgao);
      });
    }
  }

  preencheOrgaoFuncionarioParaSalvar() {
    let org = new Orgao();
    if (this.orgaosSelecionados.length > 0) {
      this.entidade.orgaosFuncionario = new Array<OrgaoFuncionario>();
      for (let i = 0; i < this.orgaosSelecionados.length; i++) {
        let orgao: any = this.orgaosSelecionados[i];
        this.orgaoService.buscar(orgao.id)
          .then(element => {
            this.entidade.orgaosFuncionario.push(new OrgaoFuncionario(element, null));
          });
      }
    }
  }

  preencherOrgaoFuncionarioSalvo() {
    if (this.entidade.orgaosFuncionario.length > 0) {
      this.entidade.orgaosFuncionario.forEach(element => {
        this.orgaosSelecionados.push(element.orgao);
      });
    }
  }

  adicionarOrgao() {
    this.orgaosSelecionados.push(new Orgao(1));
  }

  salvarOrgao(orgao: Orgao, index: number) {
    if (this.entidade.id) {
      let org: any = orgao;
      this.orgaoService.buscar(org.id)
        .then(element => {


          let filtro = new OrgaoFuncionarioFiltro();
          let codigoEntidade: number;

          filtro.orgao = org.id;
          filtro.funcionario = this.entidade.id.toString();
          this.orgaoFuncionarioService.pesquisar(filtro)
            .then(orgaoFunc => {
              if (!orgaoFunc.id) {
                this.orgaoFuncionarioService.incluir(new OrgaoFuncionario(element, this.entidade))
                  .then(() => {
                    this.toastr.success(element.nome + ' cadastrado com sucesso!');
                  });
              } else {
                this.toastr.error(element.nome + ' já está cadastrado!');
              }
            });

        });
    } else {
      this.orgaoService.buscar(orgao.id)
        .then(element => {
          this.entidade.orgaos.push(element);
          this.toastr.success(element.nome + ' incluído com sucesso ');
        });

    }
  }

  removerOrgao(orgao: Orgao, index: number) {
    let org: any = orgao;
    let filtro = new OrgaoFuncionarioFiltro();
    let codigoEntidade: number;

    filtro.orgao = org.id;
    filtro.funcionario = this.entidade.id.toString();
    this.orgaoFuncionarioService.pesquisar(filtro)
      .then(element => {
        codigoEntidade = element.id;
      });

    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.orgaosSelecionados.splice(index, 1);
        this.orgaoFuncionarioService.excluir(codigoEntidade);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

  //Credor
  //-------------------------------------------------------------------------

  preencheCredorParaSalvar() {
    if (this.credoresSelecionados.length > 0) {
      this.entidade.orgaos = new Array<Orgao>();
      for (let i = 0; i < this.credoresSelecionados.length; i++) {
        let credor: any = this.credoresSelecionados[i];
        this.entidade.credores.push(new Credor(credor.id));
      }
    }
  }

  preencherCredorSalvo() {
    if (this.entidade.credoresFuncionario.length > 0) {
      this.entidade.credoresFuncionario.forEach(element => {
        this.credoresSelecionados.push(element.credor);
      });
    }
  }

  preencheCredorFuncionarioParaSalvar() {
    let crd = new Credor();
    if (this.credoresSelecionados.length > 0) {
      this.entidade.credoresFuncionario = new Array<CredorFuncionario>();
      for (let i = 0; i < this.credoresSelecionados.length; i++) {
        let credor: any = this.credoresSelecionados[i];
        this.credorService.buscar(credor.id)
          .then(element => {
            this.entidade.credoresFuncionario.push(new CredorFuncionario(element, null));
          });
      }
    }
  }

  preencherCredorFuncionarioSalvo() {
    if (this.entidade.credoresFuncionario.length > 0) {
      this.entidade.credoresFuncionario.forEach(element => {
        this.credoresSelecionados.push(element.credor);
      });
    }
  }

  adicionarCredor() {
    this.credoresSelecionados.push(new Credor(1));
  }

  selecionarCredor(credor: Credor) {
    if (this.entidade.id) {
      let crd: any = credor;
      this.credorService.buscar(crd.id)
        .then(element => {

          let filtro = new CredorFuncionarioFiltro();
          this.credoresSelecionados.push(credor);

          filtro.credor = crd.id;
          filtro.funcionario = this.entidade.id.toString();
          this.credorFuncionarioService.pesquisar(filtro)
            .then(credorFunc => {
              if (!credorFunc.id) {
                this.credorFuncionarioService.incluir(new CredorFuncionario(element, this.entidade))
                  .then(() => {
                    this.toastr.success(element.nome + ' cadastrado com sucesso!');
                  });
              } else {
                this.toastr.error(element.nome + ' já está cadastrado!');
              }
            });

        });
    } else {
      this.credorService.buscar(credor.id)
        .then(element => {
          this.entidade.credores.push(element);
          this.toastr.success(element.nome + ' incluído com sucesso ');
        });

    }
  }

  removerCredor(credor: Credor, index: number) {
    let crd: any = credor;
    let filtro = new CredorFuncionarioFiltro();
    let codigoEntidade: number;

    filtro.credor = crd.id;
    filtro.funcionario = this.entidade.id.toString();
    this.credorFuncionarioService.pesquisar(filtro)
      .then(element => {
        codigoEntidade = element.id;
      });

    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir',
      accept: (() => {
        this.credoresSelecionados.splice(index, 1);
        this.credorFuncionarioService.excluir(codigoEntidade);
        this.toastr.success('Item excluído com sucesso!');
      })
    });
  }

}
