import { CheckListModule } from './cadastro/check-list/check-list.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { OrgaoModule } from './cadastro/orgao/orgao.module';
import { TipoDocumentoModule } from './cadastro/tipo-documento/tipo-documento.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { PaginaPrincipalComponent } from './core/pagina-principal/pagina-principal.component';
import { AcessoNaoAutorizadoComponent } from './core/acesso-nao-autorizado/acesso-nao-autorizado.component';
import { SegurancaGuard } from './seguranca/seguranca.guard';

import { SegurancaModule } from './seguranca/seguranca.module';
import { PermissaoModule } from './seguranca/permissao/permissao.module';
import { PerfilModule } from './seguranca/perfil/perfil.module';
import { UsuarioModule } from './seguranca/usuario/usuario.module';
import { AssuntoModule } from './cadastro/assunto/assunto.module';
import { ReferenciaModule } from './cadastro/referencia/referencia.module';
import { DocumentoModule } from './cadastro/documento/documento.module';
import { CaixaModule } from './cadastro/caixa/caixa.module';
import { ArmarioModule } from './cadastro/armario/armario.module';
import { PastaModule } from './cadastro/pasta/pasta.module';
import { FuncionarioModule } from './recursos-humanos/funcionario/funcionario.module';
import { CargoModule } from './recursos-humanos/cargo/cargo.module';
import { DocumentoJuridicoModule } from './cadastro/documento-juridico/documento-juridico.module';
import { RelatoriosModule } from './relatorios/relatorios.module';

const routes: Routes = [
  {
    path: '',
    redirectTo:'asjur/principal',
    pathMatch: 'full'
  },
  {
    path: 'asjur/principal',
    component: PaginaPrincipalComponent,
    canActivate:[SegurancaGuard],
  },
  {
    path: 'asjur/acesso-nao-autorizado',
    component: AcessoNaoAutorizadoComponent,
  },
  {
    path: '**',
    redirectTo: 'pagina-nao-encontrada'
  },
  {
    path: 'pagina-nao-encontrada',
    component: PaginaNaoEncontradaComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SegurancaModule,
    PermissaoModule,
    PerfilModule,
    UsuarioModule,
    TipoDocumentoModule,
    AssuntoModule,
    ReferenciaModule,
    DocumentoModule,
    CaixaModule,
    ArmarioModule,
    PastaModule,
    FuncionarioModule,
    CargoModule,
    OrgaoModule,
    DocumentoJuridicoModule,
    RelatoriosModule,
    DashboardModule,
    CheckListModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
