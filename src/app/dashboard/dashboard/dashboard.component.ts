import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

import { DashboardService } from './../dashboard.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public pieChartData: any;
  public lineChartData: any;
  public barChartData: any;
  public polarChartData: any;
  public prazo: string;
  public items: MenuItem[];
  public homeBreadcrumb: MenuItem;
  public prazos = [
    { label: 'TODOS', value: 'TODOS' },
    { label: 'PROCESSOS FINALIZADOS EM ATRASO', value: 'ATRASO' },
    { label: 'PROCESSOS FINALIZADOS NO PRAZO', value: 'PRAZO' },
  ]

  options = {
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          const dataset = data.datasets[tooltipItem.datasetIndex];
          const valor = dataset.data[tooltipItem.index];
          const label = dataset.label ? (dataset.label + ': ') : '';

          return label + this.decimalPipe.transform(valor, '1.2-2');
        }
      }
    }
  };

  constructor(
    public dashboardService: DashboardService,
    public decimalPipe: DecimalPipe) { }

  ngOnInit() {
    this.items = [
      {label:'SeinfraSin'},
      {label:'Gráfico'},
      {label:'Controle de prazos'}
    ];

    this.homeBreadcrumb = {icon: 'fa fa-home'};

    this.configurarGraficoPizza();
    this.configurarGraficoLinha();
  }

  configurarGraficoPizza() {
    this.dashboardService.estatisticaDocumentoAdvogado(this.prazo)
      .then(dados => {
        this.pieChartData = {
          labels: dados.map(dado => dado.funcionario.nome),
          datasets: [
            {
              data: dados.map(dado => dado.id),
              backgroundColor: ['#FF9900', '#109618', '#990099', '#3B3EAC', '#0099C6',
                                  '#DD4477', '#3366CC', '#DC3912']
            }
          ]
        };
      });

      this.dashboardService.estatisticaDocumentoUgb(this.prazo)
      .then(dados => {
        this.polarChartData = {
          labels: dados.map(dado => dado.ugb),
          datasets: [
            {
              data: dados.map(dado => dado.id),
              backgroundColor: ['#FF9900', '#109618']
            }
          ]
        };

      });

      this.dashboardService.estatisticaDocumentoUgb(this.prazo)
      .then(dados => {
        const meses = this.mesesDoAno();
        const totaisJuridico = this.totaisPorUgb(
          dados.filter(dado => dado.ugb === 'JURIDICO'));
        const totaisCompliance = this.totaisPorUgb(
          dados.filter(dado => dado.ugb === 'COMPLIANCE'));
        this.barChartData = {
          labels: this.adicinarNomeDoMes(meses),
          datasets: [

              {
                  label: 'Jurídico',
                  backgroundColor: '#9CCC65',
                  borderColor: '#7CB342',
                  data: totaisJuridico,
                },
                {
                  label: 'Compliance',
                  backgroundColor: '#42A5F5',
                  borderColor: '#1E88E5',
                  data: totaisCompliance,
                }
          ]
      }
      });
  }

  configurarGraficoLinha() {
    this.dashboardService.estatisticaDocumentoAdvogado(this.prazo)
      .then(dados => {
        const meses = this.mesesDoAno();
        const totaisRoberta = this.totaisPorAdvogado(
          dados.filter(dado => dado.ugb === 'ROBERTA'));
        const totaisSheila = this.totaisPorAdvogado(
          dados.filter(dado => dado.funcionario.nome === 'SHEILA'));
          const totaisMarcia = this.totaisPorAdvogado(
            dados.filter(dado => dado.funcionario.nome === 'MARCIA'));
            const totaisKariana = this.totaisPorAdvogado(
              dados.filter(dado => dado.funcionario.nome === 'KARIANA'));

        this.lineChartData = {
          labels: this.adicinarNomeDoMes(meses),
          datasets: [
            {
              label: 'Marcia',
              backgroundColor: '#109618',
              borderColor: '#109618',
              data: totaisMarcia,
            },
            {
              label: 'Roberta',
              backgroundColor: '#3B3EAC',
              borderColor: '#3B3EAC',
              data: totaisRoberta,
            },
            {
              label: 'Sheila',
              backgroundColor: '#9CCC65',
              borderColor: '#7CB342',
              data: totaisSheila,
            },

            {
              label: 'Kariana',
              backgroundColor: '#42A5F5',
              borderColor: '#1E88E5',
              data: totaisKariana,
            }
          ]
        }
      });

      this.dashboardService.estatisticaDocumentoUgb(this.prazo)
      .then(dados => {
        const meses = this.mesesDoAnoTotais();
        const totaisJuridico = this.totaisPorUgb(
          dados.filter(dado => dado.ugb === 'JURIDICO'));
        const totaisCompliance = this.totaisPorUgb(
          dados.filter(dado => dado.ugb === 'COMPLIANCE'));
        this.barChartData = {
          labels: this.adicinarNomeDoMes(meses),
          datasets: [

              {
                  label: 'Jurídico',
                  backgroundColor: '#9CCC65',
                  borderColor: '#7CB342',
                  data: totaisJuridico,
                },
                {
                  label: 'Compliance',
                  backgroundColor: '#42A5F5',
                  borderColor: '#1E88E5',
                  data: totaisCompliance,
                }
          ]
      }
      });
  }

  public totaisPorUgb(dados) {
    const totais: number[] = [];
      let total = 0;

      for (const dado of dados) {
        if (dado.ugb === 'JURIDICO') {
          total = dado.id;
          break;
        }
        if (dado.ugb === 'COMPLIANCE') {
          total = dado.id;
        }
      }

      totais.push(total);

    return totais;
  }

  public totaisPorAdvogado(dados) {
    const totais: number[] = [];
      let total = 0;

      for (const dado of dados) {
        console.log(dado.funcionario.nome);
        if (dado.funcionario.nome === 'MARCIA') {
          total = dado.id;
          break;
        }
        if (dado.funcionario.nome === 'ROBERTA') {
          total = dado.id;
          break;
        }
        if (dado.funcionario.nome === 'SHEILA') {
          total = dado.id;
          break;
        }
        if (dado.funcionario.nome === 'KARIANA') {
          total = dado.id;
          break;
        }
      }

      totais.push(total);

    return totais;
  }


// --------------------------------------------------------------------------------------------
  graficoEntradaAno() {
    this.mesesDoAno();
    this.dashboardService.estatisticaDocumentoUgb(this.prazo)
      .then(dados => {
        const meses = this.mesesDoAno();
        const entradas = this.totaisPorMes(dados.filter(dados => dados.movimentacao === 'ENTRADA'), meses);
        const saidas = this.totaisPorMes(dados.filter(dados => dados.movimentacao === 'SAIDA'), meses);

        this.barChartData = {
          labels: this.adicinarNomeDoMes(meses),
            datasets: [
                {
                    label: 'Receitas',
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: entradas
                },
                {
                    label: 'Despesas',
                    backgroundColor: '#9CCC65',
                    borderColor: '#7CB342',
                    data: saidas
                }
            ]
        }
      });
  }

  public totaisPorMes(dados, meses) {
    const totais: number[] = [];
    for (const mes of meses) {
      let total = 0;

      for (const dado of dados) {
        if (dado.mes === mes) {
          total = dado.total;
          break;
        }
      }

      totais.push(total);
    }

    return totais;
  }

  public mesesDoAno() {
    const mesAtual = new Date();
    mesAtual.setMonth(mesAtual.getMonth() + 1);

    const meses: number[] = [];

    for (let i = 1; i <= 12; i++) {
      meses.push(i);
    }
    return meses;
  }

  public mesesDoAnoTotais() {
    const mesAtual = new Date();
    mesAtual.setMonth(mesAtual.getMonth() + 1);

    const meses: number[] = [];

    for (let i = 1; i <= mesAtual.getMonth(); i++) {
      meses.push(i);
    }
    return meses;
  }

  public adicinarNomeDoMes(meses){
    const nomeMeses: string[] = [];
    for (const mes of meses) {
      let nomeMes;

      switch(mes) {
        case 1: {
          nomeMes = "Janeiro";
          break;
        }
        case 2: {
          nomeMes = "Fevereiro"
          break;
        }
        case 3: {
          nomeMes = "Março"
          break;
        }
        case 4: {
          nomeMes = "Abril"
          break;
        }
        case 5: {
          nomeMes = "Maio"
          break;
        }
        case 6: {
          nomeMes = "Junho"
          break;
        }
        case 7: {
          nomeMes = "Julho"
          break;
        }
        case 8: {
          nomeMes = "Agosto"
          break;
        }
        case 9: {
          nomeMes = "Setembro"
          break;
        }
        case 10: {
          nomeMes = "Outubro"
          break;
        }
        case 11: {
          nomeMes = "Novembro"
          break;
        }
        case 12: {
          nomeMes = "Dezembro"
          break;
        }
        default: {
          break;
        }
     }
      nomeMeses.push(nomeMes);
    }
    return nomeMeses;
  }
}
