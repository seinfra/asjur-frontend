import { HttpParams } from '@angular/common/http';
import { CrudService } from './../core/crud-service';
import { DocumentoJuridico } from 'src/app/cadastro/documento-juridico/documento-juridico';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { Injectable } from '@angular/core';

import * as moment from 'moment';

import { environment } from './../../environments/environment';

@Injectable()
export class DashboardService extends CrudService<DocumentoJuridico>{

  private baseUrl = `${environment.apiUrl}/asjur-api/graficos`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/graficos`);
  }

  processoPorMes(): Promise<Array<any>> {
    return this.http.get<Array<any>>(`${this.baseUrl}/processo/por-mes`)
      .toPromise();
  }


  estatisticaDocumentoAdvogado (prazo: string): Promise<any> {
      const params = new HttpParams()
      .append('prazo', prazo);

      return this.http.get(`${this.baseUrl}/estatistica-documento-advogado`, { params})
        .toPromise()
        .then( response => response);
  }

  estatisticaDocumentoUgb(prazo: string): Promise<any> {
    const params = new HttpParams()
    .append('prazo', prazo);

    return this.http.get(`${this.baseUrl}/estatistica-documento-ugb`, { params})
      .toPromise()
      .then( response => response);
  }

  processoPorUgb(): Promise<Array<any>> {
    return this.http.get<Array<any>>(`${this.baseUrl}/processo/por-ugb`)
      .toPromise()
      .then(response => {
        const dados = response;
        this.converterStringsParaDatas(dados);

        return dados;
      });
  }

  private converterStringsParaDatas(dados: Array<any>) {
    for (const dado of dados) {
      dado.dia = moment(dado.dia, 'YYYY-MM-DD').toDate();
    }
  }
}
