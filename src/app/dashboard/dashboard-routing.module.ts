import { SegurancaGuard } from './../seguranca/seguranca.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'asjur/dashboard/controlePrazos',
    component: DashboardComponent,
    canActivate:[SegurancaGuard],
    data: { roles: ['GRAFICO_CONTROLE_PRAZO']}
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
