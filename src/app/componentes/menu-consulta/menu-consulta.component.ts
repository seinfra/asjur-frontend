import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-consulta',
  templateUrl: './menu-consulta.component.html',
  styleUrls: ['./menu-consulta.component.css']
})
export class MenuConsultaComponent {

  @Input() rotaNovo: string;
  @Input() rotaPadrao: string;
  @Input() codigoEntidade: number;
  @Input() consultar = true;
  @Output() pesquisar = new EventEmitter();
  @Output() limpar = new EventEmitter();

  constructor(
    public router: Router,
    public toastr: ToastrService,
  ) { }

  pesquisarEntidade () {
    this.pesquisar.emit();
  }

  limparFiltroEntidade(){
    this.limpar.emit();
  }
}
