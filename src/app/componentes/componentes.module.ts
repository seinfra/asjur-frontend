import { ScrollPanelModule } from 'primeng/scrollpanel';
import { BuscarUnidadeAdministrativaComponent } from './buscar-unidade-administrativa/buscar-unidade-administrativa.component';
import { BuscarCredorComponent } from './buscar-credor/buscar-credor.component';
import { MenuConsultaComponent } from './menu-consulta/menu-consulta.component';
import { MenuAcaoComponent } from './menu-acao/menu-acao.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {FieldsetModule} from 'primeng/fieldset';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';

import { MenuFormComponent } from './menu-form/menu-form.component';
import { MenuListComponent } from './menu-list/menu-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    FieldsetModule,
    BreadcrumbModule,
    RadioButtonModule,
    DropdownModule,
    DialogModule,
    ScrollPanelModule
  ],
  declarations: [
    MenuFormComponent,
    MenuListComponent,
    MenuAcaoComponent,
    MenuConsultaComponent,

    BuscarUnidadeAdministrativaComponent,
    BuscarCredorComponent,
  ],
  exports: [
    MenuFormComponent,
    MenuListComponent,
    MenuAcaoComponent,
    MenuConsultaComponent,

    BuscarUnidadeAdministrativaComponent,
    BuscarCredorComponent,
  ]
})
export class ComponentesModule { }
