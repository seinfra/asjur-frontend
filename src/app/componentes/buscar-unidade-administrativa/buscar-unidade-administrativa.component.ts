import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';

import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { UnidadeAdministrativa } from 'src/app/cadastro/unidade-administrativa/unidade-administrativa';
import { UnidadeAdministrativaFiltro, UnidadeAdministrativaService } from 'src/app/cadastro/unidade-administrativa/unidade-administrativa.service';

@Component({
  selector: 'buscar-unidade-administrativa',
  templateUrl: './buscar-unidade-administrativa.component.html',
  styleUrls: ['./buscar-unidade-administrativa.component.css']
})
export class BuscarUnidadeAdministrativaComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  public visibleDialogEntidade: boolean;
  public selecionados: UnidadeAdministrativa[];
  public filtro = new UnidadeAdministrativaFiltro;
  public totalRegistros = 0;
  public entidadeSelecionada = new UnidadeAdministrativa();
  public codigoSelecionado: number;

  constructor(
    public entidadeService: UnidadeAdministrativaService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }


  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.entidadeSelecionada = fornecedor;
    this.codigoSelecionado = fornecedor.id;
  }

  limparFiltro () {
    this.filtro = new UnidadeAdministrativaFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
