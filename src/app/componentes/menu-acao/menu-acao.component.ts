import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-acao',
  templateUrl: './menu-acao.component.html',
  styleUrls: ['./menu-acao.component.css']
})
export class MenuAcaoComponent {

  @Input() rotaNovo: string;
  @Input() rotaPadrao: string;
  @Input() codigoEntidade: number;
  @Input() consultar = true;
  @Output() excluir = new EventEmitter();
  @Output() editar = new EventEmitter();
  @Input() permissaoNovo: boolean = true;
  @Input() permissaoEditar: boolean = true;
  @Input() permissaoConsultar: boolean = true;
  @Input() permissaoExcluir: boolean = true;

  constructor(
    public router: Router,
    public toastr: ToastrService,
  ) { }

  existeItemSelecionado (codigo:number) {
    this.editar.emit(codigo);
  }

  excluirEntidade(codigo: number){
    this.excluir.emit(codigo);
  }
}
