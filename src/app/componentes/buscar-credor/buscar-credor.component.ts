import { CredorFiltro, CredorService } from './../../core/service/credor.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Credor } from 'src/app/core/model/credor';

@Component({
  selector: 'buscar-credor',
  templateUrl: './buscar-credor.component.html',
  styleUrls: ['./buscar-credor.component.css']
})
export class BuscarCredorComponent implements OnInit {

  @Output() seleciona = new EventEmitter();

  public visibleDialogEntidade: boolean;
  public selecionados: Credor[];
  public filtro = new CredorFiltro;
  public totalRegistros = 0;
  public entidadeSelecionada = new Credor();
  public codigoSelecionado: number;

  constructor(
    public entidadeService: CredorService,
    public errorHandler : ErrorHandlerService,
  ) { }

  ngOnInit() {
  }


  showDialogEntidade(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = true;
  }

  pesquisarEntidade(pagina = 0) {
    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina =  10;

    this.entidadeService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina (event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisarEntidade(pagina);
  }

  selecionarEntidade(fornecedor){
    this.entidadeSelecionada = fornecedor;
    this.codigoSelecionado = fornecedor.id;
  }

  limparFiltro () {
    this.filtro = new CredorFiltro();
    this.totalRegistros = 0;
    this.selecionados = [];
  }

  confirmarModal(entidade){
    if(entidade){
      this.seleciona.emit(entidade);
    }
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

  cancelarModal(){
    this.limparFiltro ();
    this.selecionados = [];
    this.visibleDialogEntidade = false;
  }

}
