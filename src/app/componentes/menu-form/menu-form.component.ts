import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/api';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.component.html',
  styleUrls: ['./menu-form.component.css']
})
export class MenuFormComponent {

  @Input() rotaListar: string;
  @Input() entidade: string;
  @Input() codigoEntidade: number;
  @Input() consulta: boolean;
  @Input() formulario: FormControl;
  @Input() permissaoImprimir: boolean = true;
  @Output() salvar = new EventEmitter();
  @Output() imprimir = new EventEmitter();

  constructor(
    public router: Router,
    public confirmation: ConfirmationService,
  ) { }

  cancelar (){

    let msg = "";
    if(this.entidade['codigo']){
      msg = "Tem certeza que deseja descartar a alteração";
    }else{
      msg = "Tem certeza que deseja descartar a inclusão";
    }

    this.confirmation.confirm({
      message: msg,
      accept: (() => {
        this.router.navigate([this.rotaListar]);
      })
    });
  }

  salvarEntidade(formulario: FormControl){
    this.salvar.emit(formulario);
  }

  imprimirEntidade(codigo: number){
    this.imprimir.emit(codigo);
  }

}
