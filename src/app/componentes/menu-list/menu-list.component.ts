import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {

  @Input() rotaNovo: string;
  @Input() rotaPadrao: string;
  @Input() codigoEntidade: number;
  @Input() consultar = true;
  @Output() excluir = new EventEmitter();
  @Input() permissaoNovo: boolean = true;
  @Input() permissaoEditar: boolean = true;
  @Input() permissaoConsultar: boolean = true;
  @Input() permissaoExcluir: boolean = true;
  @Input() permissaoImprimir: boolean = true;

  constructor(
    public router: Router,
    public toastr: ToastrService,
  ) { }

  excluirEntidade(codigo:number){
    this.excluir.emit(codigo);
  }
}
