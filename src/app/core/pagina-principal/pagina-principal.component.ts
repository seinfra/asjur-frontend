import { CheckListDocumentoService } from './../../cadastro/check-list/check-list-documento.service';
import { ConfirmationService } from 'primeng/components/common/api';
import { Funcionario } from 'src/app/recursos-humanos/funcionario/funcionario';
import { FuncionarioService } from './../../recursos-humanos/funcionario/funcionario.service';
import { DocumentoJuridico } from './../../cadastro/documento-juridico/documento-juridico';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { DocumentoFiltro } from './../../cadastro/documento/documento.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MenuItem } from 'primeng/api';
import { DocumentoJuridicoService } from 'src/app/cadastro/documento-juridico/documento-juridico.service';
import { Router } from '@angular/router';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { CheckListService } from 'src/app/cadastro/check-list/check-list.service';
import { CheckList, CheckListSub, CheckListDocumento, CheckListTipo } from 'src/app/cadastro/check-list/check-list';
import { CheckListDocumentoPendente } from '../model/check-list-documento';
import { ParecerService } from '../service/parecer.service';
import { Parecer } from '../model/parecer';

@Component({
  selector: 'app-pagina-principal',
  templateUrl: './pagina-principal.component.html',
  styleUrls: ['./pagina-principal.component.css']
})
export class PaginaPrincipalComponent implements OnInit{

  public items: MenuItem[];
  public visibleDialogRecebimento = false;
  public visibleDialogAnalise = false;
  public visibleDialogCheckList = false;
  public visibleCadastroPreliminar = false;
  public visibleDiligencia = false;
  public visibleCheckList = false;
  public visibleInicioDistribuicaoList = false;
  public visibleFinalizarProcesso = false;
  public visibleRedistribuicao = false;
  public visibleDialogConfirmar = false;
  public filtro = new DocumentoFiltro;
  public quantidadeItemPorPagina: any[];
  public quantidadeItemPorPaginaSelecionada: any;
  public totalRegistros = 0;
  public contAltissima = 0;
  public contAlta = 0;
  public contMedia = 0;
  public contBaixa = 0;
  public redistribuir = false;
  public codigoEntidade: number;
  public selecionado = new DocumentoJuridico();
  public selecionados: DocumentoJuridico[];
  public funcionarios: Funcionario[];
  public checksList: CheckList[];
  public checkListSelecionado = new CheckList();
  public checkListTipoSelecionado = new CheckListTipo();
  public checksListSub: CheckListSub[];
  public checksListTipo: CheckListTipo[];
  public checkListSubSelecionado = new CheckListSub();
  public checksListDoc: CheckListDocumento[] = [];
  public documentosReceber: DocumentoJuridico[];
  public documentosReceberSelecionados: DocumentoJuridico[] = [];
  public documentosCheckSelecionados: CheckListDocumento[] = [];
  public documentosCheckList: DocumentoJuridico[];
  public documentosAnalise: DocumentoJuridico[];
  public documentosFinalizar: DocumentoJuridico[];
  public documentosInicioDistribuicao: DocumentoJuridico[];
  public loading = false;
  public rotaNovo = "/asjur/documento-juridico/novo";
  public rotaPadrao = '/asjur/documento-juridico/';
  public diligencias = [
    { label: 'CHECKLIST', value: 'CHECKLIST' },
    { label: 'MÉRITO', value: 'MERITO' },
    { label: 'MOTIVO', value: 'MOTIVO' },
  ]

  public pareceresJuridico = [
    { label: 'FAVORÁVEL', value: 'FAVORAVEL' },
    { label: 'DESFAVORÁVEL', value: 'DESFAVORÁVEL' },
    { label: 'OUTROS', value: 'OUTROS' },
  ]

  public ugbs = [
    { label: '--- Selecione ---', value: '' },
    { label: 'COMPLIANCE', value: 'COMPLIANCE' },
    { label: 'JURÍDICO', value: 'JURIDICO' },
  ]

  constructor(
    public toastrService: ToastrService,
    public documentoService: DocumentoJuridicoService,
    public checkListDocumentoService: CheckListDocumentoService,
    public funcionarioService: FuncionarioService,
    public segurancaService: SegurancaService,
    public checkListService: CheckListService,
    public errorHandler: ErrorHandlerService,
    public confirmation: ConfirmationService,
    public parecerService: ParecerService,
    public router: Router,
    public title: Title,
  ) { }

  ngOnInit() {
    this.items = [
      {label:'SeinfraSin'},
      {label:'Painel'},
      {label:'Dashboard'}
    ];

    this.title.setTitle ('SeinfraSin - Home');
    this.listarCheckList();
    this.listarDocumentos();
    this.listarInicioDistribuicao();
    this.listarDocumentoAnalisar();
    this.listarDocumentoParaFinalizar();
    this.listarDocumentoParaCheckList();
    this.listarCheckListDoc();
    this.documentoService.listaDocumentos(this.segurancaService.jwtPayload.cpf);
  }

  pesquisar(pagina = 0) {
    this.loading = true;

    this.filtro.pagina = pagina;
    this.filtro.itensPorPagina = this.quantidadeItemPorPaginaSelecionada ? this.quantidadeItemPorPaginaSelecionada.value : 10;

    this.documentoService.pesquisar(this.filtro)
      .then(resultado => {

        this.totalRegistros = resultado.total;
        this.selecionados = resultado.selecionados;
        this.loading = false;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  listarDocumentos() {
    this.loading = true;

    this.documentoService.listaDocumentoPorUsuario(this.segurancaService.jwtPayload.cpf)
      .then(resultado => {
        this.selecionados = resultado;
        this.contAltissima = 0;
        this.contAlta = 0;
        this.contMedia = 0;
        this.contBaixa = 0;

      this.selecionados.forEach(element => {
        switch(element.prioridade){
          case 'ALTISSIMA':
          this.contAltissima += 1;
          break;
          case 'ALTA':
          this.contAlta += 1;
          break;
          case 'MEDIA':
          this.contMedia += 1;
          break;
          case 'BAIXA':
          this.contBaixa += 1;
          break;
        }
        this.documentoService.contAltissima = this.contAltissima;
        this.documentoService.contAlta = this.contAlta;
        this.documentoService.contMedia = this.contMedia;
        this.documentoService.contBaixa = this.contBaixa;
      });
      })
      .catch(erro => this.errorHandler.handle(erro));
      this.loading = false;
  }

  listarDocumentoReceber(){
    this.documentoService.listaDocumentoReceber(this.segurancaService.jwtPayload.cpf)
    .then(element => {
      this.documentosReceber = element;
      if(this.documentosReceber.length > 0){
        this.visibleDialogRecebimento = true
      }
    })
  }

  listarCheckList() {
    this.checkListService.listar()
    .then(element => {
      this.checksList = element.map(c => ({ label: c.descricao, value: c.id }));
    });
  }

  listarCheckListTipo() {
    if(this.checkListSelecionado.id !== undefined){
      this.checkListService.buscarTipo(this.checkListSelecionado.id)
      .then(element => {
        this.checksListTipo = element.map(c => ({ label: c.descricao, value: c.id }));
      });
    }
  }

  listarCheckListSub() {
    if(this.checkListTipoSelecionado.id !== undefined){
      this.checkListService.buscarSub(this.checkListTipoSelecionado.id)
      .then(element => {
        this.checksListSub = element.map(c => ({ label: c.descricao + ' / ' + c.complexidade + ' - ' + c.prazo, value: c.id }));
      });
    }
  }

  listarFuncionarioJuridico(){
    if(this.redistribuir){
      this.funcionarioService.funcionarioJuridico()
      .then( element => {
        this.funcionarios = element.map(c => ({ label: c.nome, value: c.id }));
      });
    }else{
      this.funcionarios = null;
    }
  }

  listarDocumentoAnalisar(){
    this.documentoService.listaDocumentoAnaliseCoordenador()
    .then(element => {
      this.documentosAnalise = element;
      this.funcionarioService.funcionarioPorCpf(this.segurancaService.jwtPayload.cpf)
      .then(funcionario => {
        if(funcionario != null && funcionario.cargo.nome === 'COORDENADOR' && this.documentosAnalise.length > 0){
          this.visibleDialogAnalise = true
        }
      });
    })
  }

  listarDocumentoParaCheckList(){
    this.documentoService.listaDocumentoParaCheckList(this.segurancaService.jwtPayload.cpf)
    .then(element => {
      this.documentosCheckList = element;
      this.funcionarioService.funcionarioPorCpf(this.segurancaService.jwtPayload.cpf)
      .then(funcionario => {
        if(funcionario != null && funcionario.cargo.nome === 'ADVOGADO' && this.documentosCheckList.length > 0){
          this.visibleDialogCheckList = true
        }
      });
    })
  }

  listarDocumentoParaFinalizar(){
    this.documentoService.listaDocumentoParaFinalizar()
    .then(element => {
      this.documentosFinalizar = element;
      this.funcionarioService.funcionarioPorCpf(this.segurancaService.jwtPayload.cpf)
      .then(funcionario => {
        if(funcionario != null && funcionario.ugb === 'APOIO' && this.documentosFinalizar.length > 0){
          this.visibleFinalizarProcesso = true
        }
      });
    })
  }

  listarCheckListDoc() {
    if(this.checkListSubSelecionado.id !== undefined){
      this.checkListService.buscarDocumentacao(this.checkListSubSelecionado.id, this.selecionado.id)
      .then(element => {
        this.checksListDoc = element;
      });
    }
  }

  listarInicioDistribuicao(){
    this.loading = true;
    this.documentoService.listaInicioDistribuicao()
    .then(element => {

      this.documentosInicioDistribuicao = element;
      this.funcionarioService.funcionarioPorCpf(this.segurancaService.jwtPayload.cpf)
      .then(funcionario => {
        if(funcionario != null && funcionario.ugb === 'APOIO' && this.documentosInicioDistribuicao.length > 0){
          this.visibleInicioDistribuicaoList = true
        }
      });
      this.loading = false;
    })
  }

  limparFiltro () {
    this.filtro = new DocumentoFiltro();
    this.pesquisar();
  }

  aprovarProcesso(entidade: DocumentoJuridico){
    this.confirmation.confirm({
      message: 'Tem certeza que deseja aprovar o processo nº ' + entidade.numeroProcesso + ' ?',
      accept: (() => {
    entidade.status = 'Aprovado coordenador/aguardando finalização apoio'
    let doc = this.documentosAnalise.indexOf(entidade);
    this.documentosAnalise.splice(doc, 1);
    entidade.dataConclusaoReal = new Date();
    this.documentoService.alterar(entidade);

    this.toastrService.success('Processo ' + entidade.numeroProcesso + ' aprovado');
        if(this.documentosAnalise.length == 0){
          this.visibleDialogAnalise = false;
        }
      })
    });
  }

  reprovarProcesso(entidade: DocumentoJuridico){
    this.selecionado = entidade;
    if(!this.selecionado.funcionario){
      this.selecionado.funcionario = new Funcionario();
    }
    this.visibleDiligencia = true;
  }

  diligenciaProcesso(entidade: DocumentoJuridico){
    entidade.dataConclusao = null;
    entidade.redistribuicao = this.redistribuir;
    entidade.prevento = true;

    if(this.redistribuir){
      entidade.status = 'Diligência com redistribuição/análise advogado'
    }else{
      entidade.status = 'Diligência/análise advogado'
    }
    let doc = this.documentosAnalise.indexOf(entidade);
    this.documentosAnalise.splice(doc, 1);
    this.removerSelecionados(entidade);
    let pareceres = new Array<Parecer>();
    entidade.pareceres.forEach(element => {
      element.manifestacaoJuridica = true;
      pareceres.push(element);
    });
    this.parecerService.incluir(new Parecer(entidade, true));

    entidade.pareceres = new Array<Parecer>();
    entidade.pareceres = pareceres;
    entidade.observacao = null;
    this.documentoService.alterarDocumento(entidade);

    this.toastrService.error('Processo ' + entidade.numeroProcesso + ' reprovado');
    this.visibleDiligencia = false;
    if(this.documentosAnalise.length == 0){
      this.visibleDialogAnalise = false;
    }
    this.incluirSelecionado(entidade);
    this.listarDocumentos();
  }

  distribuicaoIndividual(entidade: DocumentoJuridico){
    this.confirmation.confirm({
    message: 'Tem certeza que deseja distribuir o processo ' + entidade.numeroProcesso + ' ?',
    accept: (() => {

        this.documentoService.distribuicaoIndividual(entidade.id)
        .then(() => {
          this.toastrService.success('Distribuição individual concluída com sucesso!');
        });

        this.documentoService.buscar(entidade.id)
        .then( doc => {
          if(doc.funcionario !== null){
          this.funcionarioService.buscar(doc.funcionario.id).then(func => {
            entidade.funcionario = func;
          });
        }
        this.documentosInicioDistribuicao.slice(this.documentosInicioDistribuicao.findIndex(x => x == entidade), 1);
        this.selecionados.slice(this.selecionados.findIndex(x => x == entidade), 1);

        });
        this.listarDocumentos();
        this.router.navigate(['/asjur/principal']);
        location.reload();
      })
    });
  }

  distribuirEmBloco(){
    this.loading = true;

    this.documentoService.distribuicaoEmBloco()
    .then(element => {
      this.documentosInicioDistribuicao = element;
    });
    this.toastrService.success('Distribuição em bloco concluída com sucesso!');
    this.visibleInicioDistribuicaoList = false;
    this.listarDocumentos();
    this.router.navigate(['/asjur/principal']);
    location.reload();
    this.loading = false;
  }

  redistribuicaofuncionario(entidade: DocumentoJuridico){
    this.selecionado = entidade;
    if(!this.selecionado.funcionario){
      this.selecionado.funcionario = new Funcionario();
    }
    this.visibleRedistribuicao = true;
    this.funcionarioService.funcionarioJuridico()
    .then( element => {
      this.funcionarios = element.map(c => ({ label: c.nome, value: c.id }));
    });
  }

  receberProcesso(){
    this.visibleDialogRecebimento = false;
    this.selecionado.status = 'Em análise advogado'
    this.selecionado.dataRecebimentoAdvogado = new Date();
    this.documentosReceberSelecionados.forEach(element => {
      element.dataRecebimentoAdvogado = new Date();
      element.status = 'Em análise advogado'
      this.documentoService.alterar(element);
      this.listarDocumentos();
      }
    );
    this.toastrService.success('Confirmado o recebimento do processo ' + this.selecionado.numeroProcesso)
  }

  redistribuirProcesso(entidade: DocumentoJuridico){
    entidade.redistribuicao = true;
    this.documentoService.alterar(entidade);
    let doc = this.documentosAnalise.indexOf(entidade);

    this.funcionarioService.buscar(entidade.funcionario.id).then(func => {
      entidade.funcionario = func;
    });
    this.documentosAnalise.splice(doc, 1);
    this.documentosAnalise.push(entidade);
    this.toastrService.success('Redistribuição concluída com sucesso');
    this.listarDocumentos();
    this.visibleRedistribuicao = false;
  }

  confirmarDocumento(entidade: DocumentoJuridico){
    entidade.status = 'Aguardando análise advogado'
    this.removerSelecionados(entidade);
    this.documentoService.alterar(entidade)
    .then( () => {
      this.toastrService.success('CheckList concluído com sucesso!');
    });
    this.visibleCheckList = false;
    this.listarDocumentoReceber();

    this.incluirSelecionado(entidade);
  }

  cadastroPreliminar(entidade: DocumentoJuridico){
    this.selecionado = entidade;
    if(!this.selecionado.funcionario){
      this.selecionado.funcionario = new Funcionario();
    }
    this.limparCamposDocumentoPendente();
    this.visibleCadastroPreliminar = true;
  }

  confirmarCadastroPreliminar(){
    this.selecionado.status = 'Aguardando checklist'

    let documentoPendente = new CheckListDocumentoPendente();

    documentoPendente.checkList = this.checkListSelecionado;
    documentoPendente.checkListTipo = this.checkListTipoSelecionado;
    documentoPendente.checkListSub = this.checkListSubSelecionado;
    documentoPendente.documento = this.selecionado;
    this.checkListDocumentoService.incluir(documentoPendente);

    this.selecionado.subTipo = documentoPendente.checkListSub.id;
    this.documentoService.alterarDocumento(this.selecionado)
    .then( () => {
      this.toastrService.success('Cadastro preliminar concluído com sucesso!');
    });
    this.visibleCadastroPreliminar = false;
  }

  limparCamposDocumentoPendente() {
    this.checkListSelecionado = new CheckList();
    this.checkListTipoSelecionado = new CheckListTipo();
    this.checkListSubSelecionado = new CheckListSub();
    this.checksListDoc = new Array<CheckListDocumento>();
  }

  checkList(entidade: DocumentoJuridico){
    entidade.documentosPendente.forEach(
    element => {
      this.checkListSelecionado = element.checkList;
      this.checkListTipoSelecionado = element.checkListTipo;
      this.checkListSubSelecionado = element.checkListSub;
      if(element.ativo && element.checkListDocumento){
        element.checkListDocumento.ativo = element.ativo;
    }
      this.listarCheckListTipo();
      this.listarCheckListSub();
      this.listarCheckListDoc();
    });

    this.selecionado = entidade;
    if(!this.selecionado.funcionario){
      this.selecionado.funcionario = new Funcionario();
    }
    this.visibleCheckList = true;
    this.visibleDialogCheckList = false;
  }

  pendenciaDocumento(entidade: DocumentoJuridico){
    entidade.diligencia = 'CHECKLIST';
    entidade.status = 'Aguardando checklist/Pendência'
    let documentoPendente = new CheckListDocumentoPendente();

    this.checkListDocumentoService.buscarPorDocumentoPendente(this.selecionado.id);
      this.documentosCheckSelecionados.forEach(
        element => {
          documentoPendente.checkList = this.checkListSelecionado;
          documentoPendente.checkListTipo = this.checkListTipoSelecionado;
          documentoPendente.checkListSub = this.checkListSubSelecionado;
          documentoPendente.checkListDocumento = element;
          documentoPendente.ativo = element.ativo;
          documentoPendente.documento = entidade;
          this.checkListDocumentoService.incluir(documentoPendente);
        }
      );
    let pareceres = new Array<Parecer>();
    entidade.pareceres.forEach(element => {
      element.manifestacaoJuridica = true;
      pareceres.push(element);
    });
    entidade.pareceres = new Array<Parecer>();
    entidade.pareceres = pareceres;

    this.removerSelecionados(entidade);
    this.documentoService.alterar(entidade)
    .then( () => {
      this.toastrService.error('CheckList com pendência de documento!');
    });
    this.limparCamposDocumentoPendente();
    this.documentoService.buscar(entidade.id)
    .then(documento =>{
      entidade = documento;
    });
    this.listarDocumentos();
    this.visibleCheckList = false;
    this.incluirSelecionado(entidade);
  }

  finalizarProcesso(entidade: DocumentoJuridico){
    this.confirmation.confirm({
      message: 'Tem certeza que deseja finalizar o processo nº ' + entidade.numeroProcesso + ' ?',
      accept: (() => {
        this.removerSelecionados(entidade);
        entidade.status = 'Processo finalizado'
        let doc = this.documentosFinalizar.indexOf(entidade);
        this.documentosFinalizar.splice(doc, 1);
        this.documentoService.alterar(entidade)
        .then( () => {
          this.toastrService.success('Processo finalizado com sucesso!');
          this.listarDocumentos();
        });
        if(this.documentosFinalizar.length == 0){
          this.visibleFinalizarProcesso = false;
        }
      })
    });
  }

  selecionarEntidade (entidade){
    if(entidade.distribuir){
      this.documentosReceberSelecionados.push(entidade);
    }else{
      let doc = this.documentosReceberSelecionados.indexOf(entidade);
      this.documentosReceberSelecionados.splice(doc, 1);
    }
    this.selecionado = entidade;
    if(!this.selecionado.funcionario){
      this.selecionado.funcionario = new Funcionario();
    }
    this.codigoEntidade = entidade.id;
    this.visibleDialogConfirmar = true;
  }

  SelecionarDocumento(entidade: CheckListDocumento){
    if(entidade.ativo === true){
        this.documentosCheckSelecionados.push(entidade);
    }else{
        let doc = this.documentosCheckSelecionados.indexOf(entidade);
        this.documentosCheckSelecionados.splice(doc, 1);
        this.checkListDocumentoService.buscarPorDocumento(entidade.id, this.selecionado.id)
        .then(element => {
          this.checkListDocumentoService.excluir(element.id);
        });
      }
  }

  removerSelecionados(entidade: DocumentoJuridico) {
    this.selecionados.forEach( element => {
      if(element.id === entidade.id) {
        this.selecionados.splice(this.selecionados.indexOf(element), 1);
      }
    });
  }

  incluirSelecionado(entidade: DocumentoJuridico){
    this.selecionados.push(entidade);
  }

  editar (codigo: number) {
    this.router.navigate([this.rotaPadrao + codigo]);
  }

}
