import { OrgaoFuncionarioService } from './service/orgao-funcionario.service';
import { CredorService } from './service/credor.service';
import { ParecerService } from './service/parecer.service';
import { CheckListDocumentoService } from './../cadastro/check-list/check-list-documento.service';
import { CheckListService } from './../cadastro/check-list/check-list.service';
import { DropdownModule } from 'primeng/dropdown';
import { FuncionarioService } from './../recursos-humanos/funcionario/funcionario.service';
import { ButtonModule } from 'primeng/button';
import { InputMaskModule } from 'primeng/inputmask';
import { DialogModule } from 'primeng/dialog';
import { DataLocaleService } from 'src/app/core/data-locale-service';
import { ArmarioService } from './../cadastro/armario/armario.service';
import { HeaderComponent } from './header/header.component';
import { PerfilService } from './../seguranca/perfil/perfil.service';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Title } from '@angular/platform-browser';
import localePt from '@angular/common/locales/pt';

import { ToastrModule } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule }   from '@angular/forms';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { AccordionModule } from 'primeng/accordion';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { NgxLoadingModule } from 'ngx-loading';

import { MenuComponent } from './menu/menu.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { AcessoNaoAutorizadoComponent } from './acesso-nao-autorizado/acesso-nao-autorizado.component';

import { ErrorHandlerService } from './error-handler.service';
import { SegurancaService } from '../seguranca/seguranca.service';
import { SistemaHttp } from '../seguranca/sistema-http';
import { PermissaoService } from '../seguranca/permissao/permissao.service';
import { UsuarioService } from '../seguranca/usuario/usuario.service';
import { SideBarComponent } from './side-bar/side-bar.component';
import { SidebarModule } from 'primeng/primeng';
import { TipoDocumentoService } from '../cadastro/tipo-documento/tipo-documento.service';
import { AssuntoService } from '../cadastro/assunto/assunto.service';
import { ReferenciaService } from '../cadastro/referencia/referencia.service';
import { DocumentoService } from '../cadastro/documento/documento.service';
import { CaixaService } from '../cadastro/caixa/caixa.service';
import { PastaService } from '../cadastro/pasta/pasta.service';
import { CargoService } from '../recursos-humanos/cargo/cargo.service';
import { OrgaoService } from '../cadastro/orgao/orgao.service';
import { DocumentoJuridicoService } from '../cadastro/documento-juridico/documento-juridico.service';
import { TableModule } from 'primeng/table';
import { RelatoriosService } from '../relatorios/relatorios.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { MunicipioService } from './service/municipio.service';

registerLocaleData(localePt);

@NgModule({
  imports: [
    //Importação do Angular
    CommonModule,
    RouterModule,
    SidebarModule,

    //Importação do PrimeNG
    ConfirmDialogModule,
    BreadcrumbModule,
    DialogModule,
    InputMaskModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    TableModule,
    InputTextModule,
    FormsModule,
    ScrollPanelModule,
    AccordionModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgxLoadingModule,

    //Importação de terceiros
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  declarations: [
    MenuComponent,
    PaginaNaoEncontradaComponent,
    PaginaPrincipalComponent,
    AcessoNaoAutorizadoComponent,
    HeaderComponent,
    SideBarComponent
  ],
  exports: [
    MenuComponent,
    ToastrModule,
    ConfirmDialogModule,
    TableModule

  ],
  providers: [
    //Provider Angular
    Title,
    { provide: LOCALE_ID, useValue: 'pt' },

    //Provider PrimeNG
    ConfirmationService,

    //Provider Criados
    DataLocaleService,
    SistemaHttp,
    ErrorHandlerService,
    SegurancaService,
    PermissaoService,
    UsuarioService,
    PerfilService,
    TipoDocumentoService,
    AssuntoService,
    ReferenciaService,
    DocumentoService,
    CaixaService,
    ArmarioService,
    PastaService,
    FuncionarioService,
    CargoService,OrgaoService,
    DocumentoJuridicoService,
    RelatoriosService,
    DashboardService,
    CheckListService,
    CheckListDocumentoService,
    OrgaoFuncionarioService,
    ParecerService,
    CredorService,
    MunicipioService
  ]
})
export class CoreModule { }
