export class Municipio {
  id: number;
  nome: string;
  codigoIBGE: string;

  constructor(id?: number, nome?: string, codigoIBGE?: string){
      this.id = id;
      this.nome = nome;
      this.codigoIBGE = codigoIBGE;
  }

}
