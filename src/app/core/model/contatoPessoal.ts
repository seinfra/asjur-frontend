export class ContatoPessoal {
  codigo: number;
  email:string;
  telefone: string;
  celular: string;

  constructor(codigo?: number, email?: string, telefone?: string, celular?: string){
      this.codigo = codigo;
      this.email = email;
      this.telefone = telefone;
      this.celular = celular;
  }

}
