import { DocumentoJuridico } from 'src/app/cadastro/documento-juridico/documento-juridico';

export class Parecer {
  id: number;
  documento = new DocumentoJuridico();
  manifestacaoJuridica: boolean;

  constructor(documento?: DocumentoJuridico, manifestacaoJuridica?: boolean){
    this.documento = documento;
    this.manifestacaoJuridica = manifestacaoJuridica;
  }
}
