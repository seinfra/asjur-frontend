import { CheckList, CheckListTipo, CheckListSub, CheckListDocumento } from 'src/app/cadastro/check-list/check-list';
import { DocumentoJuridico } from 'src/app/cadastro/documento-juridico/documento-juridico';
  
  export class CheckListDocumentoPendente {
    id: number;
    descricao: string;
    ativo: boolean;
    documento = new DocumentoJuridico();
    checkList = new CheckList();
    checkListTipo = new CheckListTipo();
    checkListSub = new CheckListSub();
    checkListDocumento = new CheckListDocumento();

    constructor(id?: number, descricao?: string){
      this.id = id;
      this.descricao = descricao;
    }
}


