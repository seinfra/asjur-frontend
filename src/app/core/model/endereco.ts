import { Municipio } from './municipio';

export class Endereco {
  codigo: number;
  logradouro: string;
  numero: number;
  complemento: string;
  bairro: string;
  cep: string;
  estado: string;
  municipio = new Municipio();

  constructor(codigo?:number, logradouro?:string, numero?:number, complemento?: string, bairro?: string ){
    this.codigo = codigo;
    this.logradouro = logradouro;
    this.numero = numero;
    this.complemento = complemento;
    this.bairro = bairro;
  }
}
