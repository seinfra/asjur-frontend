import { Funcionario } from './../../recursos-humanos/funcionario/funcionario';

export class PlanoFerias {

  ano: number;
  dataInicial: Date;
  dataFinal: Date;
  dataSolicitacao: Date;
  quantidadeDias: number;
  observacao: string;
  statusFerias: string;

  dataInicialSaldo: Date;
  dataFinalSaldo: Date;
  dataSolicitacaoSaldo: Date;
  quantidadeDiasSaldo: number;
  observacaoSaldo: string;
  statusFeriasSaldo: string;

  dataInicialSaldoRestante: Date;
  dataFinalSaldoRestante: Date;
  dataSolicitacaoSaldoRestante: Date;
  quantidadeDiasSaldoRestante: number;
  observacaoSaldoRestante: string;
  statusFeriasSaldoRestante: string;

  funcionario = new Funcionario();

  constructor(ano?: number, dataInicial?: Date, dataFinal?: Date, quantidadeDias?: number,
    dataInicialSaldo?: Date, dataFinalSaldo?: Date, quantidadeDiasSaldo?: number, observacao?: string,
    statusFerias?: any, dataSolicitacao?: Date, dataSolicitacaoSaldo?: Date, observacaoSaldo?: string,
    dataSolicitacaoSaldoRestante?: Date, dataInicialSaldoRestante?: Date, dataFinalSaldoRestante?: Date,
    quantidadeDiasSaldoRestante?: number, observacaoSaldoRestante?: string, statusFeriasSaldo?: string, statusFeriasSaldoRestante?: string){
      this.ano = ano;
      this.dataInicial = dataInicial;
      this.dataFinal = dataFinal;
      this.quantidadeDias = quantidadeDias;
      this.dataInicialSaldo = dataInicialSaldo;
      this.dataFinalSaldo = dataFinalSaldo;
      this.quantidadeDiasSaldo = quantidadeDiasSaldo;
      this.observacao = observacao;
      this.statusFerias = statusFerias;
      this.dataSolicitacao = dataSolicitacao;
      this.dataSolicitacaoSaldo = dataSolicitacaoSaldo,
      this.observacaoSaldo = observacaoSaldo;
      this.dataSolicitacaoSaldoRestante = dataSolicitacaoSaldoRestante;
      this.dataInicialSaldoRestante = dataInicialSaldoRestante;
      this.dataFinalSaldoRestante = dataFinalSaldoRestante;
      this.quantidadeDiasSaldoRestante = quantidadeDiasSaldoRestante;
      this.observacaoSaldoRestante = observacaoSaldoRestante;
      this.statusFeriasSaldo = statusFeriasSaldo;
      this.statusFeriasSaldoRestante = statusFeriasSaldoRestante;
  }
}
