import { Assunto } from './../../cadastro/assunto/assunto';
import { Funcionario } from './../../recursos-humanos/funcionario/funcionario';

export class Requerimento {

  dataInicial: Date;
  dataFinal: Date;
  observacao: string;
  assunto = new Assunto();
  funcionario = new Funcionario();

  constructor(dataInicial?: Date, dataFinal?: Date, observacao?: string, funcionario?: Funcionario, assunto ?: Assunto){
      this.dataInicial = dataInicial;
      this.dataFinal = dataFinal;
      this.observacao = observacao;
      this.funcionario = funcionario;
      this.assunto = assunto;
  }
}
