export class Contato {
  codigo: number;
  email:string;
  telefone: string;
  ramal: string;

  constructor(codigo?: number, email?: string, telefone?: string, ramal?: string){
      this.codigo = codigo;
      this.email = email;
      this.telefone = telefone;
      this.ramal = ramal;
  }

}
