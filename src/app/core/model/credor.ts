export class Credor {
  id: number;
  nome: string;
  cpfCnpj: string;

  constructor(id?: number, nome?: string, cpfCnpj?: string){
      this.id = id;
      this.nome = nome;
      this.cpfCnpj = cpfCnpj;
  }

}
