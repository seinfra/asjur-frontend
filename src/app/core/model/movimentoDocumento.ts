import { Funcionario } from '../../recursos-humanos/funcionario/funcionario';
import { DocumentoJuridico } from 'src/app/cadastro/documento-juridico/documento-juridico';

export class MovimentoDocumento {

  dataHoraEnvio: Date;
  dataRecebimento: Date;
  observacao: string;
  parecer: string;
  documento = new DocumentoJuridico();
  funcionarioOrigem = new Funcionario();
  funcionarioDestino = new Funcionario();

}
