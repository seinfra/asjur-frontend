import { DocumentoJuridico } from './../../cadastro/documento-juridico/documento-juridico';
import { DocumentoJuridicoService } from 'src/app/cadastro/documento-juridico/documento-juridico.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { LogoutService } from 'src/app/seguranca/logout.service';
import { SegurancaService } from './../../seguranca/seguranca.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public selecionados: DocumentoJuridico[];
  public alertas = 0;

  constructor(
    public segurancaService: SegurancaService,
    public documentoService: DocumentoJuridicoService,
    public router: Router,
    public logoutService: LogoutService,
    public errorHandler: ErrorHandlerService,
    ) { }

  ngOnInit() {}

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/asjur/login']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
