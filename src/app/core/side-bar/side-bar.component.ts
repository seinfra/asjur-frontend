import { DocumentoFiltro } from './../../cadastro/documento/documento.service';
import { ErrorHandlerService } from './../error-handler.service';
import { DocumentoJuridicoService } from './../../cadastro/documento-juridico/documento-juridico.service';
import { Component, OnInit, Input } from '@angular/core';
import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { DocumentoJuridico } from 'src/app/cadastro/documento-juridico/documento-juridico';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  public items: MenuItem[];
  public visibleDialogContato = true;
  public receber: boolean;
  public filtro = new DocumentoFiltro;
  public quantidadeItemPorPagina: any[];
  public quantidadeItemPorPaginaSelecionada: any;
  public totalRegistros = 0;
  public contAltissima = 0;
  public contAlta = 0;
  public contMedia = 0;
  public contBaixa = 0;
  @Input() selecionados: DocumentoJuridico[];
  public rotaNovo = "/asjur/documento-juridico/novo";
  public rotaPadrao = '/asjur/documento-juridico/';
  display;

  constructor(
    public segurancaService: SegurancaService,
    public documentoService: DocumentoJuridicoService,
    public errorHandler: ErrorHandlerService,
    public router: Router,

  ) { }

  ngOnInit() {}

  esconderMenu() {
    this.display = false;
  }

  mostrarMenu() {
    this.display = true;
  }

  editar (codigo: number) {
    this.router.navigate([this.rotaPadrao + codigo]);
  }

}

