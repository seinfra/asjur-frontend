import { OrgaoFuncionario } from './../../recursos-humanos/funcionario/funcionario';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';

export class OrgaoFuncionarioFiltro {
  orgao: string;
  funcionario: string;
}

@Injectable({
  providedIn: 'root'
})
export class OrgaoFuncionarioService extends CrudService<OrgaoFuncionario>{

  private baseUrl = `${environment.apiUrl}/asjur-api/orgao-funcionario`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/orgao-funcionario`);
  }


  pesquisar(filtro: OrgaoFuncionarioFiltro): Promise<any> {

    let params = new HttpParams();

    if(filtro.orgao) {
      params = params.append('orgao', filtro.orgao);
    }

    if(filtro.funcionario) {
      params = params.append('funcionario', filtro.funcionario);
    }

    return this.http.get<any>(this.baseUrl, { params })
    .toPromise()
    .then( response => response);
  }
}
