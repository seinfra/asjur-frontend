import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Municipio } from '../model/municipio';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService extends CrudService<Municipio>{

  private baseUrl = `${environment.apiUrl}/asjur-api/municipio`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/municipio`)
  }

}
