import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';
import { Parecer } from '../model/parecer';

@Injectable({
  providedIn: 'root'
})
export class ParecerService extends CrudService<Parecer>{

  private baseUrl = `${environment.apiUrl}/asjur-api/parecer`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/parecer`)
  }

}
