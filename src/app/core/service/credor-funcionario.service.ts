import { OrgaoFuncionario, CredorFuncionario } from './../../recursos-humanos/funcionario/funcionario';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';

export class CredorFuncionarioFiltro {
  credor: string;
  funcionario: string;
}

@Injectable({
  providedIn: 'root'
})
export class CredorFuncionarioService extends CrudService<CredorFuncionario>{

  private baseUrl = `${environment.apiUrl}/asjur-api/credor-funcionario`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/credor-funcionario`);
  }


  pesquisar(filtro: CredorFuncionarioFiltro): Promise<any> {

    let params = new HttpParams();

    if(filtro.credor) {
      params = params.append('credor', filtro.credor);
    }

    if(filtro.funcionario) {
      params = params.append('funcionario', filtro.funcionario);
    }

    return this.http.get<any>(this.baseUrl, { params })
    .toPromise()
    .then( response => response);
  }
}
