import { HttpParams } from '@angular/common/http';
import { Credor } from './../model/credor';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { SistemaHttp } from 'src/app/seguranca/sistema-http';
import { CrudService } from 'src/app/core/crud-service';

export class CredorFiltro {
  id: number;
  nome: string;
  cpfCnpj: string;
  pagina = 0;
  itensPorPagina: number;
}

@Injectable({
  providedIn: 'root'
})

export class CredorService extends CrudService<Credor>{

  private baseUrl = `${environment.apiUrl}/asjur-api/credor`;

  constructor(protected http: SistemaHttp) {
    super(http, `${environment.apiUrl}/asjur-api/credor`)
  }

  pesquisar(filtro: CredorFiltro): Promise<any> {

    let params = new HttpParams({
      fromObject: {
        page: filtro.pagina.toString(),
        size: filtro.itensPorPagina.toString()
      }
    });

    if(filtro.id) {
      params = params.append('id', filtro.id.toString());
    }

    if(filtro.nome) {
      params = params.append('nome', filtro.nome);
    }

    if(filtro.cpfCnpj) {
      params = params.append('cpfCnpj', filtro.cpfCnpj);
    }

    return this.http.get<any>(this.baseUrl, { params })
      .toPromise()
      .then( response => {
        const resultado = {
          selecionados: response.content,
          total: response.totalElements
         }
        return resultado;
      });
  }

}
