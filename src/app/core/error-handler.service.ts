import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private toastr: ToastrService,
    private router: Router
  ) { }

  handle (errorResponse: any) {
    let msg: string = "";

    if(typeof errorResponse == 'string') {
      msg = errorResponse;
    } else if (errorResponse instanceof HttpErrorResponse && errorResponse.status >= 400 && errorResponse.status <= 499) {

      msg = 'Ocorreu um erro ao processar sua solicitação';

      if(errorResponse.status === 403){
        msg = 'Você não tem permissão para executar esta ação';
      }

      try {
        errorResponse.error.forEach(function (value) {
          msg = value.mensagemUsuario + ", " + msg;
        });
        msg = msg.substr(0,msg.length - 2);
      } catch (e) { }
    } else {
      msg = 'Erro ao processar serviço remoto';
    }

    this.toastr.error(msg);
  }

}
