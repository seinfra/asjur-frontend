import { SistemaHttp } from '../seguranca/sistema-http';

export class CrudService<T> {

  constructor(protected http: SistemaHttp, protected API_URL) { }

  incluir(entidade: T): Promise<T> {
    return this.http.post<T>(this.API_URL, entidade)
      .toPromise();
  }

  alterar(entidade: T): Promise<T> {
    return this.http.put<T>(`${this.API_URL}/${entidade['id']}`, entidade)
      .toPromise()
      .then(response => response );
  }

  excluir(codigo: number): Promise<void> {
    return this.http.delete(`${this.API_URL}/${codigo}`)
      .toPromise()
      .then(() => null);
  }

  buscar (codigo: number): Promise<any> {
    return this.http.get(`${this.API_URL}/${codigo}`)
      .toPromise()
      .then( response => response);
  }

  listar(): Promise<any> {

    return this.http.get(this.API_URL+"/listar")
      .toPromise()
      .then( response => response);
  }

}
