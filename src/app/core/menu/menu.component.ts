import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SegurancaService } from 'src/app/seguranca/seguranca.service';
import { LogoutService } from 'src/app/seguranca/logout.service';
import { ErrorHandlerService } from '../error-handler.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  private exibindoMenu = false;
  private fundo = true;
  private login = true;
  private display;

  constructor(
    private segurancaService: SegurancaService,
    private router: Router,
    private logoutService: LogoutService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  exibirMenu() {

    if(this.router.url === '/asjur/login'){
      this.exibindoMenu = false;
      this.fundo = true;
      this.login = true;
    }else{
      if(this.login){
        this.exibindoMenu = true;
        this.fundo = false;
        this.login = false;
      }
    }
    return this.router.url !== '/asjur/login';
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/asjur/login']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  esconderMenu() {
    this.display = false;
  }

  mostrarMenu() {
    this.display = true;
  }

}
