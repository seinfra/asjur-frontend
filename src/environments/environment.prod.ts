export const environment = {
  production: true,
  apiUrl: window["cfgApiBaseUrl"],
  apiUrlEx: 'http://seinfrasin5.seinfra.ce.gov.br/sad-backend',
  tokenWhitelistedDomains: [ new RegExp('http://seinfrasin5.seinfra.ce.gov.br') ],
  tokenBlacklistedRoutes: [ new RegExp('\/oauth\/token') ]
};


