export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080/asjur-backend',
  apiUrlEx: 'http://localhost:8080/sad-backend',
  tokenWhitelistedDomains: [ new RegExp('localhost:8080') ],
  tokenBlacklistedRoutes: [ new RegExp('\/oauth\/token') ]
};
